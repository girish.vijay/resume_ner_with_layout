#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Script to get parsed text split by tags. """

import os
import re
import json
import glob
import yaml
import click
import unicodedata
from tqdm import tqdm
import pandas as pd
import math

is_clean = False
# TODO
# tag tenures
# check for work_exp and edu_ headings in secondary headings
# tag projects

IGNORE_NAMES = {"1086", "1420", "1437", "1496", "2059", "2087"}
IGNORE_EMAIL = {"1086", "1128", "1215", "1283", "1347", "1430",
                "1460", "2429"}


def _parsed_data(resume_id: str, parsed_pdfs_dir: str,
                 parsed_resumes: set) -> dict:
    """
    Returns parsed info from the resume.
    """
    if parsed_pdfs_dir + resume_id + "_ner.json" in parsed_resumes:
        return json.load(open(parsed_pdfs_dir + resume_id + "_ner.json", "r"))
    else:
        return None


def clean_text(text: str) -> str:
    """
    Function to remove unicode from text.
    """
    text = unicodedata.normalize("NFKD", text).encode("ascii", "ignore")
    text = text.decode()
    text = " ".join(text.split()).strip()
    return text


def _fields_to_tag(parsed_pdf_json: dict) -> dict:
    """
    Returns key-value pair of fields to be tagged by the NER model.
    """
    fields_to_tag = []
    for field, sub_field in FIELDS.items():
        if field not in parsed_pdf_json:
            continue
        if not sub_field:
            fields_to_tag.append((field, parsed_pdf_json[field]))
        elif isinstance(sub_field, str):
            fields_to_tag.append((field, parsed_pdf_json[field][sub_field]))
        elif isinstance(sub_field, list):
            for ent in parsed_pdf_json[field]:
                for _sub_field in sub_field:
                    if _sub_field in ent:
                        fields_to_tag.append((f"{field}_{_sub_field}",
                                              ent[_sub_field]))
    # fix when projects are actually employers
    # if "employer" not in parsed_pdf_json:
    if "project" in parsed_pdf_json:
        for emp in parsed_pdf_json["project"]:
            if "company" in emp:
                fields_to_tag.append(("project_company_name",
                                      emp["company"]))
            if "role" in emp:
                fields_to_tag.append(("project_role",
                                      emp["role"]))
            if 'title' in emp:
                fields_to_tag.append(("project_title",
                                      emp["title"]))


    return fields_to_tag


def span_overlap(span1: list, span2: list) -> list:
    """
    Overlap between two spans.
    """
    return list(range(max(span1[0], span2[0]), min(span1[-1], span2[-1])))


def _reject_overlapping_tags(tags, show_rejects: bool = False) -> list:
    """
    Rejects overlapping tags.
    """
    final_tags = []
    if tags:
        final_tags = [tags[0]]
        for t in tags[1:]:
            overlap = False
            for ft in final_tags:
                if span_overlap(t[-1], ft[-1]):
                    overlap = True
            if not overlap:
                final_tags.append(t)
            else:
                if show_rejects:
                    print(f"Rejected: {repr(t[1])}")

    if final_tags:
        final_tags = sorted(final_tags, key=lambda x: x[-1][0])

    return final_tags


def _closest_span(span, tags):
    return sorted([abs(t[1][0] - span[0]) for t in tags])[0]

def _extract_word_level_bound_box(parsed_text, heading_tags, tags):
    invalid_layout = []
    final_txt = ''
    final_wrds_layout = []
    map_char_bound_box = []
    ind_map = 0

    x_mx = max([max(i[1][0], i[1][2]) for i in parsed_text['whole_text_layout']])
    y_mx = max([max(i[1][1], i[1][3]) for i in parsed_text['whole_text_layout']])
    for txt_layout in parsed_text['whole_text_layout']:
        max_h = 1000
        factx, facty = max_h / x_mx,\
                       max_h/ y_mx
        if is_clean:
            cln_txt = clean_text(txt_layout[0])
        else:
            cln_txt = txt_layout[0]

        if cln_txt:
            cln_txt_sub = re.sub('[^a-zA-Z0-9]','',cln_txt)
            for tg, wrd in heading_tags:
                if cln_txt_sub == re.sub('[^a-zA-Z0-9]','',wrd):
                    if tg not in HEADINGS_TO_KEEP:
                        tg = "others_heading"

                    tags.append((tg,cln_txt, (len(final_txt), len(final_txt) +len(cln_txt)) ))
                    heading_tags.remove((tg, wrd))
                    break


            final_txt = final_txt+ cln_txt + ' -NL- '

            wrds = cln_txt.split()
            len_wrds = [len(wrd) for wrd in wrds]
            tot_chrs = sum(len_wrds)
            ratio_wrds = [i/tot_chrs for i in len_wrds]


            x1, y1, x2, y2 = txt_layout[1]



            if y1 < 0:
                y1 = 1
            if x1 < 0:
                x1 = 1

            if y2 < 0:
                y2 = 2
            if x2 < 0:
                x2 = 2

            if y2 <= y1:
                invalid_layout.append({'y':txt_layout})
                y2, y1 = y1,y2
            if x2 <= x1 :
                invalid_layout.append({'x':txt_layout})
                x2, x1 = x1, x2


            x_span = x2 - x1
            x1_wrd = x1
            ind_sp = 1
            for wrd, rat in zip(wrds, ratio_wrds):

                x2_wrd = x1_wrd + math.floor(rat*x_span)
                final_wrds_layout.append([wrd, [ int(x1_wrd *factx), \
                                                 int(y1 *facty), int(x2_wrd *factx),\
                                                 int(y2 *facty)] ])

                map_char_bound_box[ind_map: ind_map+ len(wrd) +1] = [[int(x1_wrd * factx), \
                                            int(y1 * facty), int(x2_wrd * factx), \
                                            int(y2 * facty)]] * (len(wrd) +1)
                ind_map = ind_map + len(wrd) + 1
                x1_wrd = x1 + x_span * sum(ratio_wrds[:ind_sp])
                ind_sp += 1

                if rat > 1 or int(x1_wrd * factx) > 1000 or x_span<0 or rat <0 :
                    aa =1



            x2_wrd = x1_wrd + 1
            if rat > 1 or int(x1_wrd * factx) > 1000 or x_span <0 or y2*facty>1003:
                aa =1
            final_wrds_layout.append(['-NL-', [int(x1_wrd * factx), \
                                            int(y1 * facty), int(x2_wrd * factx), \
                                            int(y2 * facty)]])
            map_char_bound_box[ind_map: ind_map + len('-NL-') + 1] = [[int(x1_wrd * factx), \
                                            int(y1 * facty), int(x2_wrd * factx), \
                                            int(y2 * facty)]] * (len('-NL-') +1)
            ind_map = ind_map + len('-NL-') + 1


        # Split bound box among words


    return final_txt, final_wrds_layout, tags, map_char_bound_box, invalid_layout

def lst_unique(seq):
   # order preserving
   checked = []
   for e in seq:
       if e not in checked:
           checked.append(e)
   return checked

def chk_valid_tag(m, parsed_text):
    s = m.span()
    if s[0] > 0:
        if re.findall('[a-zA-Z]', parsed_text[s[0]-1]):
            return False
    if s[1] < len(parsed_text):
        if re.findall('[a-zA-Z]', parsed_text[s[1]]):
            return False

    return True



def _txt_grouping_for_tokenization(tagging_info: list,
                                   parsed_text: dict) -> list:
    """
    Groups parsed_text based on their tags.
    """
    # heading tags
    heading_tags = []
    for ind in parsed_text["headings"]:
        if is_clean:
            text = clean_text(parsed_text["headings"][ind][0])
        else:
            text = parsed_text["headings"][ind][0]
        heading_tags.append((parsed_text["headings"][ind][1],text))


    # tag spans
    #parsed_text = \
    #    "\n".join([txt["text"] for txt in parsed_text["parsed_info"][1:]])
    tags = []
    parsed_text, parsed_wrds_layout, tags, map_char_bound_box, invalid_layout = \
        _extract_word_level_bound_box(parsed_text, heading_tags, tags)

    prev_tag = []
    for tag, text in tagging_info + heading_tags:
        if is_clean:
            text = clean_text(text)
        if clean_text(text):
            if tag.endswith("heading"):
                if tag not in HEADINGS_TO_KEEP:
                    tag = "others_heading"
                '''
                
                try:
                    for m in re.finditer(f"\n{text}\n", parsed_text):
                        _span = m.span()
                        tags.append((tag, text, (_span[0] + 1, _span[1] - 1)))
                except Exception:
                    pass
                    
                '''
            else:
                if tag.endswith("date"):
                    if prev_tag and (prev_tag[-1][0].startswith("employer") or
                       prev_tag[-1][0].startswith("education")):
                        if "/" in text:
                            text = text.split("/")[1]
                        elif text[-4:].isdigit():
                            text = text[-4:]
                        elif text[-2:].isdigit():
                            text = text[-2:]
                        for m in re.finditer(re.escape(text), parsed_text):
                            if _closest_span(m.span(), prev_tag) < TENURE_MAX_DIST:
                                tags.append(("TENURE", text, m.span()))
                                break
                else:
                    try:
                        found = False
                        if tag == "education_degree":
                            pattern = r"\b{}\b".format(text)
                        else:
                            pattern = re.escape(text)
                        for m in re.finditer(pattern, parsed_text):
                            if chk_valid_tag(m,parsed_text):
                                tags.append((tag, text, m.span()))
                                found = True
                                if tag.startswith("employer") or tag.startswith("education"):
                                    prev_tag.append((tag, m.span(), text))
                                #break
                        if not found:
                            txt = text.upper()
                            if tag == "education_degree":
                                pattern = r"\b{}\b".format(txt)
                            else:
                                pattern = re.escape(txt)
                            for m in re.finditer(pattern, parsed_text):
                                if chk_valid_tag(m, parsed_text):
                                    tags.append((tag, txt, m.span()))
                                    found = True
                                    if tag.startswith("employer") or tag.startswith("education"):
                                        prev_tag.append((tag, m.span(), txt))
                                    #break
                        # add newlines as proposals
                        if not found:
                            txt_splits = text.split(" ")
                            txt_proposals = []
                            if len(txt_splits) > 1:
                                for i in range(len(txt_splits)-1):
                                    txt_proposals.append(" ".join(txt_splits[:i+1]) +
                                                         " -NL- " +
                                                         " ".join(txt_splits[i + 1:]))
                            for txt in txt_proposals:
                                if tag == "education_degree":
                                    pattern = r"\b{}\b".format(txt)
                                else:
                                    pattern = re.escape(txt)
                                for m in re.finditer(pattern, parsed_text):
                                    if chk_valid_tag(m, parsed_text):
                                        tags.append((tag, txt, m.span()))
                                        found = True
                                        if tag.startswith("employer") or tag.startswith("education"):
                                            prev_tag.append((tag, m.span(), txt))
                                        #break
                        if tag == "name" and not found:
                            fnd = 0
                            for txt in  text.split(' '):
                                if len(clean_text(txt))>2:
                                    for m in re.finditer(re.escape(txt), parsed_text):
                                        tags.append((tag, txt, m.span()))
                                        fnd  = 1
                                    if fnd == 1:
                                        break
                    except Exception as e:
                        pass
                        # print(str(e))
    tags = sorted(list(set(tags)), key=lambda x: x[-1][0])
    tags = _reject_overlapping_tags(tags)

    # text grouping
    text_tags = []
    start_ix = 0
    for tag in tags:
        tag, span = tag[0].upper(), tag[-1]
        if span[0] == 0:
            text_tags.append((parsed_text[span[0]:span[1]], tag,\
                              lst_unique(map_char_bound_box[span[0]:span[1]]) ))
            start_ix = span[1]
        else:
            start_ix_lay = start_ix + 1 if parsed_text[start_ix] == ' ' else start_ix

            end_ix_lay = span[0] - 1 if parsed_text[span[0]-1] == ' ' else span[0]

            text_tags.append((parsed_text[start_ix: span[0]], "MISC",\
                              lst_unique(map_char_bound_box[start_ix_lay:end_ix_lay])))
            text_tags.append((parsed_text[span[0]: span[1]], tag,\
                              lst_unique(map_char_bound_box[span[0]:span[1]])))
            start_ix = span[1]

    if parsed_text[start_ix:]:
        start_ix_lay = start_ix + 1 if parsed_text[start_ix] == ' ' else start_ix
        text_tags.append((parsed_text[start_ix:], "MISC",\
                          lst_unique(map_char_bound_box[start_ix_lay:]) ))

    return text_tags, parsed_wrds_layout, invalid_layout


def _markdown(text_tags: list) -> str:
    """
    Generates markdown for easy viewing of tagging
    """
    txt_so_far = ""
    for text, tag, lay in text_tags:
        if tag == "MISC":
            txt_so_far += text
        else:
            txt_so_far += f"**`{text}`[{tag}]**"

    return re.sub("\n", "\n\n", txt_so_far)


@click.command()
@click.option("--tagged-data", help="path to tagged json")
@click.option("--parsed-pdfs-dir", help="parsed pdfs dir")
@click.option("--ignore-pdfs-dir", help="ignore pdfs dir")

def main(tagged_data, parsed_pdfs_dir, ignore_pdfs_dir):
    """
    Script to get parsed text split by tags. Also creates markdowns to
    easily view the taggings.

    Arguments:
        tagged_data - path to tagged json
        parsed_pdfs_dir - parsed pdfs dir
    """
    data_dir = "../data/"


    if not tagged_data:
        tagged_data = '../data/tagged.json'

    if not parsed_pdfs_dir:
        parsed_pdfs_dir = '../data/parsed_pdfs_ner/'

    if not ignore_pdfs_dir:
        ignore_pdfs_dir = '../data/manual_tagged_analysis_filtered.csv'

    ignore_pdfs = pd.read_csv(ignore_pdfs_dir)

    ignore_pdfs = ignore_pdfs[ignore_pdfs['is_valid_manual'] == 0]
    ignore_pdfs_id = list(ignore_pdfs['id'])

    invalid_layout_dict = {}
    invalid_layout_dict['id'] = []
    invalid_layout_dict['txt_layout'] = []

    dict_extra_misses = dict()
    # manually tagged data
    print("\nLoading manually tagged data...")
    with open(tagged_data, "r") as f:
        tagged_data = json.load(f)
    print("Loaded!")

    RESUMES_TO_IGNORE = ignore_pdfs_id
    # parsed resumes
    parsed_resumes = set(glob.glob(parsed_pdfs_dir + "*.json"))

    markdown_dir = os.path.join(data_dir, "markdowns")
    text_tags_dir = os.path.join(data_dir, "text_tags")
    layout_tags_dir = os.path.join(data_dir, "layout_tags")

    print("Creating text-tags and markdowns...")
    n, n_skipped, names_missing, emails_missing = len(tagged_data), 0, [], []
    unique_emails, n_duplicate_resumes, _email, emails = {}, 0, None, []
    for i in tagged_data.keys():
        resume_id = str(i)
        if int(resume_id) not in RESUMES_TO_IGNORE:

            tagging_info = _fields_to_tag(tagged_data.get(resume_id, {}))



            parsed_text = \
                _parsed_data(resume_id, parsed_pdfs_dir, parsed_resumes)
            if tagging_info and parsed_text and \
                    parsed_text.get('whole_text_layout')\
                    and len(parsed_text['whole_text_layout'])>10:

                text_tags, text_layout, invalid_layout = \
                    _txt_grouping_for_tokenization(tagging_info, parsed_text)


                if invalid_layout:
                    invalid_layout_dict['id'].append(resume_id)
                    invalid_layout_dict['txt_layout'].append(invalid_layout)


                # check if name was found
                found_name = False
                for text, tag, lay in text_tags:
                    if tag == "NAME":
                        found_name = True
                        break
                # check employer
                found_employer = False
                for text, tag, lay in text_tags:
                    if tag.startswith("EMPLOYER"):
                        found_employer = True
                        break
                # check emails
                found_email, is_duplicate = False, False
                for text, tag, lay in text_tags:
                    if tag == "EMAILS_EMAIL":
                        found_email = True
                        if text.lower() not in unique_emails:
                            unique_emails[text.lower()] = resume_id
                            _email = text.lower()
                        else:
                            n_duplicate_resumes += 1
                            is_duplicate = True
                        break

                
                if not found_name and tagged_data[resume_id].get('name'): #and resume_id not in IGNORE_NAMES:
                    names_missing.append(resume_id)
                elif not found_email and not found_employer:# and \
                   #resume_id not in IGNORE_EMAIL:
                    emails_missing.append(resume_id)
                #else:


                if not is_duplicate and found_name:
                    emails.append(_email)
                    with open(f"{text_tags_dir}/{resume_id}.json", "w") as f:
                        json.dump(text_tags, f)
                    md_text = _markdown(text_tags)
                    with open(f"{markdown_dir}/{resume_id}.md", "w") as f:
                        f.write(md_text)

                    with open(f"{layout_tags_dir}/{resume_id}.json", "w") as f:
                        json.dump(text_layout, f)

            else:
                n_skipped += 1
        else:
            print(resume_id,'..')

    print(f"{len(names_missing)} resumes skipped becuase name was missing!")
    print(f"{len(emails_missing)} resumes skipped becuase email was missing!")
    print(f"{n_duplicate_resumes} resumes skipped becuase they were duplicates!")
    print(f"{n_skipped} resumes skipped because tagging_info or " +
          "parsed_text was missing!")
    with open("../data/emails.txt", "w") as f:
        f.write("\n".join(sorted(emails)))
    with open("../data/names_missing.txt", "w") as f:
        f.write("\n".join(sorted(names_missing)))
    with open("../data/emails_missing.txt", "w") as f:
        f.write("\n".join(sorted(emails_missing)))
    print(f"{len(os.listdir(text_tags_dir))} successful!")
    pd.DataFrame(invalid_layout_dict).to_csv('invalid_layouts.csv')




if __name__ == "__main__":
    # load config
    with open("../config/conf.yaml", "r") as f:
        conf = yaml.safe_load(f)
    FIELDS = conf["tagging"]["fields"]
    RESUMES_TO_IGNORE = conf["tagging"]["resumes_to_ignore"]
    HEADINGS_TO_KEEP = conf["tagging"]["headings_to_keep"]
    TENURE_MAX_DIST = conf["tagging"]["tenure_distance_threshold"]
    main()
