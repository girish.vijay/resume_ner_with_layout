###############################################################################
# Tenure detection in resumes
###############################################################################

import re
from datetime import datetime
from collections import namedtuple

# Tenure object
Tenure = namedtuple('Tenure', ['text', 'span', 'is_current',
                               'pattern_ix', 'month', 'year', 'txt_around',
                               'tenure_class', 'span_in_tenure'])

# class Tenure(Tenure):
#     def __repr__(self):
#         return f"{self.text} | {self.tenure_class} [{self.month}/{self.year}; {self.pattern_ix}; {self.span}] txt={self.txt_around}"

#     def __str__(self):
#         return f"{self.text} | {self.tenure_class} [{self.month}/{self.year}; {self.pattern_ix}; {self.span}]"


# Tenure regex strings
MONTHS_SHORT = r'jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec'
MONTHS_LONG = r'january|february|march|april|may|june|july|august|september|october|november|december'
MONTHS = f'{MONTHS_SHORT}|{MONTHS_LONG}'
YEAR_FULL = r'198[0-9]|199[0-9]|20[0|1][0-9]|2020'
YEAR_SHORT = r'0[0-9]|1[0-9]|9[0-9]|20'
YEARS = f'{YEAR_FULL}|{YEAR_SHORT}'
DATE = r'0[1-9]|[1|2][0-9]|3[0|1]'

# Max distance between two tenures to be considered a pair
TENURE_PAIR_MAX_DIST = 12

# Tenure regex patterns
tenure_patterns = \
    {'TXTMON_TXTMON_YEARFULLSHORT': r'({month})(\D[^+0-9]{{0,3}})({month})\s?({yr})\b'.format(month=MONTHS, yr=YEARS),
     'TXTMON_DT_YEARFULL': r'({month})(\,?\s\d{{1,2}})(\D[^+0-9]{{0,3}})?({yr})'.format(month=MONTHS, yr=YEAR_FULL),
     'TXTMON_YEARFULLSHORT': r'({month})([^+a-z0-9]{{0,3}})?({yr})'.format(month=MONTHS, yr=YEARS),
     'YEARFULLSHORT_TXTMON': r'\b({yr})(( | ?\- ?| ?– ?){{0,1}})?({month})\b'.format(yr=YEARS, month=MONTHS),
     'MON_YEARFULL': r'\b(0?[1-9]|1[0|1|2])([\/\s]{{1,2}})({yr})\b'.format(yr=YEAR_FULL),
     'YEARFULL_MON': r'\b({yr})([\/\s\-\.\–]{{1,2}})(0?[1-9]|1[0|1|2])\b'.format(yr=YEAR_FULL),
     'MON_YEARSHORT': r'\b(0?[1-9]|1[0|1|2])([\/\s]{{1,2}})({yr})\b'.format(yr=YEAR_SHORT),
     'DT_MON_YEARFULL': r'\b({dt})([\/\.\s])(0?[1-9]|1[0|1|2])([\/\.\s])({yr})\b'.format(dt=DATE, yr=YEAR_FULL),
     'DT_MON_YEARSHORT': r'\b({dt})([\/\.\s])(0?[1-9]|1[0|1|2])([\/\.\s])({yr})\b'.format(dt=DATE, yr=YEAR_SHORT),
     'YEARFULL': r'(\b|[\-\–\(\[])({yr})'.format(yr=YEAR_FULL),
     'YEARFULL_YEARSHORT': r'\b({yr_full})(\s|[\.\/\-\–\(\[\,])({yr_srt})\b'.format(yr_full=YEAR_FULL, yr_srt=YEAR_SHORT),
     'CURRENT_AFTER': r'\b(present(ly)?|till\s?date|till\s?time|still continuing|still employed|till\s?now|onwards|to\s?date|ongoing|today|current(ly)?|to till|now|to\s?current|as of date)\b',
     'CURRENT_BEFORE': r'\b(served till|from|since)\b'
     }


def span_overlap(span1, span2):
    """overlap between two spans"""
    return list(range(max(span1[0], span2[0]), min(span1[-1], span2[-1])))


def find_tenures(txt):
    """finds tenures in text"""
    tenures = []
    for pattern_ix, t in tenure_patterns.items():
        for _t in re.finditer(t, txt):
            _tenure = _t.group()
            if _tenure:
                span = _t.span()
                if pattern_ix == 'TXTMON_TXTMON_YEARFULLSHORT':
                    months = re.findall(MONTHS, _tenure)
                    year = re.findall('[0-9]+', _tenure)
                    tenures.append(Tenure(txt[span[0]: span[1]],
                                          (span[0], span[0] + len(months[0])),
                                          False, pattern_ix, 'n/a', 'n/a',
                                          'n/a', 'n/a', 'n/a'))
                    tenures.append(Tenure(txt[span[0]: span[1]],
                                          (span[0] + len(months[0]) + 1, span[1]),
                                          False, pattern_ix, 'n/a', 'n/a',
                                          'n/a', 'n/a', 'n/a'))
                    continue
                if pattern_ix == 'YEARFULL_YEARSHORT':
                    years = re.findall(r'\d+', _tenure)
                    tenures.append(Tenure(txt[span[0]: span[1]],
                                          (span[0], span[0] + len(years[0])),
                                          False, pattern_ix, 'n/a', 'n/a',
                                          'n/a', 'n/a', 'n/a'))
                    tenures.append(Tenure(txt[span[0]: span[1]],
                                          (span[0] + len(years[0]) + 1, span[1]),
                                          False, pattern_ix, 'n/a', 'n/a',
                                          'n/a', 'n/a', 'n/a'))
                elif pattern_ix == 'MON_YEARFULL' or pattern_ix == 'MON_YEARSHORT' or \
                        pattern_ix == 'YEARFULL':
                    # _tenure = _tenure[1:]
                    span = (span[0] + 1, span[1])
                tenures.append(Tenure(_tenure, span, False, pattern_ix,
                                      'n/a', 'n/a', 'n/a', 'n/a', 'n/a'))
    return tenures


def reject_overlapping_tenures(tenures, show_rejects=False):
    """rejects overlapping tenures"""
    final_tenures = []
    if tenures:
        final_tenures = [tenures[0]]
        for t in tenures[1:]:
            overlap = False
            for ft in final_tenures:
                if span_overlap(t.span, ft.span):
                    overlap = True
            if not overlap:
                final_tenures.append(t)
            else:
                if show_rejects:
                    print(f'Rejected: {repr(t.text)}{t.span}')

    if final_tenures:
        final_tenures = sorted(final_tenures, key=lambda x: x.span[0])

    return final_tenures


def get_single_paired_tenures(tenures, txt):
    """tries to group tenures"""
    single_tenures, paired_tenures = [], []
    n = len(tenures)
    i = 0
    while i < n:
        if i + 1 < n:
            if tenures[i + 1].span[0] - tenures[i].span[1] < TENURE_PAIR_MAX_DIST and \
               not re.findall(r'[\,\&\:]|and', txt[tenures[i].span[1]: tenures[i + 1].span[0]]):
                if i + 2 < n and tenures[i].pattern_ix == 'CURRENT_BEFORE' and \
                    tenures[i + 2].span[0] - tenures[i + 1].span[1] < TENURE_PAIR_MAX_DIST and \
                   tenures[i + 2].pattern_ix != 'CURRENT_BEFORE':
                    paired_tenures.append((tenures[i + 1], tenures[i + 2]))
                    i += 2
                elif i + 2 < n and tenures[i + 1].pattern_ix == 'CURRENT_AFTER' and \
                        not tenures[i + 2].pattern_ix.startswith('CURRENT') and \
                        tenures[i + 2].span[0] - tenures[i].span[1] < TENURE_PAIR_MAX_DIST:
                    paired_tenures.append((tenures[i], tenures[i + 2]))
                    i += 2
                else:
                    if tenures[i + 1].pattern_ix != 'CURRENT_BEFORE' and \
                       tenures[i].pattern_ix != 'CURRENT_AFTER' and \
                       ((tenures[i + 1].pattern_ix == 'CURRENT_AFTER' or
                         tenures[i].pattern_ix == 'CURRENT_BEFORE') or
                        ((tenures[i + 1].year > tenures[i].year) or
                         (tenures[i + 1].year == tenures[i].year and
                          tenures[i + 1].month >= tenures[i].month))):
                        paired_tenures.append((tenures[i], tenures[i + 1]))
                        i += 1
            else:
                if tenures[i].pattern_ix[:7] != 'CURRENT':
                    single_tenures.append(tenures[i])
        elif i == n - 1:
            single_tenures.append(tenures[i])
        i += 1

    return single_tenures, paired_tenures


def assign_month_year_to_tenure(tenure):
    """assigns month and year to Tenure"""
    dt = None
    if tenure.pattern_ix == 'TXTMON_DT_YEARFULL' or \
       tenure.pattern_ix == 'TXTMON_TXTMON_YEARFULLSHORT':
        dt = datetime.strptime(
            tenure.text[:3] + ' ' + tenure.text[-2:], '%b %y')
    elif tenure.pattern_ix == 'TXTMON_YEARFULLSHORT':
        dt = datetime.strptime(
            tenure.text[:3] + ' ' + tenure.text[-2:], '%b %y')
    elif tenure.pattern_ix == 'MON_YEARFULL':
        yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime(yr_mnth[0] + ' ' + yr_mnth[1][-2:], '%m %y')
    elif tenure.pattern_ix == 'YEARFULL_MON':
        yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime(yr_mnth[1] + ' ' + yr_mnth[0][-2:], '%m %y')
    elif tenure.pattern_ix == 'MON_YEARSHORT':
        yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime(yr_mnth[0] + ' ' + yr_mnth[1], '%m %y')
    elif tenure.pattern_ix == 'DT_MON_YEARFULL':
        dt_yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime(dt_yr_mnth[0] + ' ' + dt_yr_mnth[1] + ' ' + dt_yr_mnth[2][-2:],
                               '%d %m %y')
    elif tenure.pattern_ix == 'DT_MON_YEARSHORT':
        dt_yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime(dt_yr_mnth[0] + ' ' + dt_yr_mnth[1] + ' ' + dt_yr_mnth[2],
                               '%d %m %y')
    elif tenure.pattern_ix == 'YEARFULL':
        yr_mnth = re.findall('[0-9]+', tenure.text)
        dt = datetime.strptime('01 ' + yr_mnth[0][-2:], '%m %y')
    elif tenure.pattern_ix == 'YEARFULLSHORT_TXTMON':
        yr = re.findall('[0-9]+', tenure.text)
        mnth = re.findall('[a-z]+', tenure.text)
        dt = datetime.strptime(mnth[0][:3] + ' ' + yr[0][-2:], '%b %y')
    elif tenure.pattern_ix == 'YEARFULL_YEARSHORT':
        dt = datetime.strptime('01 ' + tenure.text[-2:], '%m %y')

    if dt:
        tenure = Tenure(tenure.text, tenure.span, tenure.is_current,
                        tenure.pattern_ix, dt.month, dt.year, 'n/a', 'n/a', 'n/a')
    else:
        tenure = Tenure(tenure.text, tenure.span, True,
                        tenure.pattern_ix, 'n/a', 'n/a', 'n/a', 'n/a', 'n/a')

    return tenure


def standardize_tenures(tenures):
    """standardizes tenures"""
    return [assign_month_year_to_tenure(t) for t in tenures]


def _reject_paired_tenures(tenure_pairs):
    """rejects paired tenures

    for example, both tenures can't have is_current=True
    """
    _tenure_pairs = []
    for t in tenure_pairs:
        if t[1].pattern_ix.startswith('CURRENT'):
            if t[0].pattern_ix.startswith('CURRENT'):
                continue
            else:
                _tenure_pairs.append(t)
        elif not t[0].pattern_ix.startswith('CURRENT'):
            if t[1].year > t[0].year:
                _tenure_pairs.append(t)
            elif t[1].year == t[0].year and t[1].month > t[0].month:
                _tenure_pairs.append(t)
        else:
            _tenure_pairs.append(t)

    return _tenure_pairs


def _reject_single_tenures(single_tenures):
    """rejects single tenures"""
    return [t for t in single_tenures
            if t.pattern_ix != 'CURRENT_AFTER' and t.pattern_ix != 'CURRENT_BEFORE']


def _clean_txt(txt):
    return ' '.join(re.findall(r'\w+', txt.lower())).strip()


def txt_around_tenure(txt, tenure_span, n_lines_to_pred=2, n_lines_to_keep=2):
    """returns text around tenures"""
    txt_before = txt[: tenure_span[0]]
    txt_splits = txt_before.split('\n')
    final_txt_before1 = '\n'.join(txt_splits[-1 * n_lines_to_pred:])
    final_txt_before2 = '\n'.join(txt_splits[-1 * n_lines_to_keep:])
    txt_after = txt[tenure_span[1]:]
    txt_splits = txt_after.split('\n')
    final_txt_after1 = '\n'.join(txt_splits[: n_lines_to_pred])
    final_txt_after2 = '\n'.join(txt_splits[: n_lines_to_keep])
    final_txt1 = final_txt_before1 + \
        _clean_txt(txt[tenure_span[0]: tenure_span[1]]) + \
        final_txt_after1
    final_txt2 = final_txt_before2 + \
        _clean_txt(txt[tenure_span[0]: tenure_span[1]]) + \
        final_txt_after2
    final_txt2_span = (len(final_txt_before2), len(final_txt_before2) +
                       len(_clean_txt(txt[tenure_span[0]: tenure_span[1]])))

    return final_txt1, (final_txt2, final_txt2_span)


def FindTenurePairs(pdf_parsed_txt, show_rejects=False, show_time=False):
    """finds single and paired tenures in parsed resume text"""
    tenures = \
        reject_overlapping_tenures(find_tenures(pdf_parsed_txt), show_rejects)
    tenures = standardize_tenures(tenures)
    single_tenures, paired_tenures = \
        get_single_paired_tenures(tenures, pdf_parsed_txt)
    single_tenures = _reject_single_tenures(single_tenures)
    paired_tenures = _reject_paired_tenures(paired_tenures)
    # find text around tenure and classify it
    txts_to_pred = []
    txts_to_keep = []
    n_single_tenures = len(single_tenures)
    for i, t in enumerate(single_tenures + paired_tenures):
        if i < n_single_tenures:
            txt1, (txt2, txt2_span) = \
                txt_around_tenure(pdf_parsed_txt, t.span, 2, 2)
        else:
            txt1, (txt2, txt2_span) = \
                txt_around_tenure(pdf_parsed_txt, (t[0].span[0], t[1].span[1]), 2, 2)
        txts_to_pred.append(txt1)
        txts_to_keep.append((txt2, txt2_span))

    return single_tenures, paired_tenures
