**`SAP Basis Consultant`[EMPLOYER_ROLE]**

Yaseen

Mobile: **`+91-8698343189`[PHONES_PHONE]**

Email id: **`Yaseen.sap9@gmail.com`[EMAILS_EMAIL]**

Technical Summary:

Total 5+ years IT experience of which SAP basis experience is 4+ experience

as a SAP Basis & Netweaver Consultant (ABAP+JAVA Stack). .

Hands on experience in SAP 4.7EE, ECC 6.0, NW2004/04s, Experience in both

Implementation and Support projects on Operating Systems like HP-Unix,

Linux, AIX,db2 and Oracle Server database systems

Excellent Skills in SAP Support with strong experience in SAP ECC

6.0, ECC 5.0/4.7EE

Sound experience in defining, configuring and maintaining the Transport

Management System (TMS) environment.

Well versed in all aspects of Client management Creating, Deleting and

Copying Clients in various modes.

Experience on BOBJ administration.

Experience in User administration, Client administration, TMS administration,

Spool administration, Profile Maintenance.

Experience in OS administration, performing Kernel upgrades, Applying

patches to the system, Applying OSS Notes, Database Administration, Spool

administration.

Applying SAP LICENSE, Applying SNOTES and Jco-Rfc destination

configuration.

Background jobs scheduling, Performance monitoring and tuning.

ADS configuration, Scot configuration.

Applying support Pack stack,ADD-ONs,Kernal upgrade both on Windows and

UNIX environment

Experience in using Profile Generator (PFCG) for creation and maintenance of

Roles/Activity groups according to the business requirements and using SAP

supplied user role template.

Good working knowledge in J2EE administration tools like Config tool, Visual

Administrator, SLD and NWA.

Expertise in patch administration like applying SAP Kernel upgrade and

upgrade of SPAM/SAINT.

Good interpersonal skills, effective communication, presentation and

excellent team player with team building capabilities.

Worked in a production support environment (24/7) supporting SAP Systems.

Education:

**`Master of Computer Application`[EDUCATION_DEGREE]** from **`Osmania University`[EDUCATION_INSTITUTE]**.

Technical Skills:

ERP

SAP R/3 4.7 EE, ECC 5.0/6.0,

OS

Windows NT/2000/XP/2003, HP-UX 11.23, IBM-AIX,

SUN Solaris

Databases

SQL Server 2000, IBM DB6 and Oracle 9i/10g,

Packages

MS Office XP/2003, MS Outlook XP/2003

Desktop/Laptops Excellent troubleshooting skill can solve any problem

related to OS & applications.

Work Experience:

Organization - Designation - Duration

**`IBM India Pvt Ltd`[EMPLOYER_COMPANY_NAME]** - **`Senior SAP Consultant`[EMPLOYER_ROLE]** - June **`2015`[TENURE]** till date

**`Atos India Pvt ltd`[EMPLOYER_COMPANY_NAME]** - **`SAP Consultant`[EMPLOYER_ROLE]** - April **`2013`[TENURE]** May **`2015`[TENURE]**

**`Prospecta Software Pvt Ltd`[EMPLOYER_COMPANY_NAME]** - **`SAP Basis Consultant`[EMPLOYER_ROLE]** - Aug **`2012`[TENURE]** Nov - **`2012`[TENURE]**

**`Consenttech Solutions`[EMPLOYER_COMPANY_NAME]** - **`Associate Consultant`[EMPLOYER_ROLE]** - Oct **`2010`[TENURE]** Jul **`2012`[TENURE]**

SAP Projects:

Project 1

Client1 : ESTEE LAUDER

Period : June 2015 Till date

Designation : **`SAP Basis Consultant`[EMPLOYER_ROLE]**

Client Description:

The company began in 1946 when Estee Lauder and her husband Joseph Lauder

began producing cosmetics in New York City. Theyfirst carried only four products:

Cleansing Oil, Skin Lotion, Super Rich All-purpose Creme, and Creme Pack. Two

years later, in 1948 they established theirfirst department store account with Saks

Fifth Avenue in New York.

Over the next 15 years, they expanded the range and continued to sell their

products in the United States. In 1960, the company started itsfirst international

account in the London department store Harrods. The following year it opened an

office in Hong Kong.

Responsibilities

Importing Bairfile in Bobj3.1 and importing promotion management jobs from

one system to other system in Bobj 4.1

Applying BOBJ 3.1 Support packs from SP 3 to SP 5.

Lumira installation add on installation on BOBJ 4.1

Performed Design studio upgrade from 1.4 to 1.6 on Bobj 4.1

SmarOps upgrade from version 6 support pack 0 to version 6 support pack 7

Downloading patches of download basket from SAP Download Manager

Manage system configurations.

Performed EHP upgrades from EHP 6 to EHP 7.

Performed Support pack upgrades in both ABAP and JAVA stacks.

Performed Add-ons upgrade.

Performed system refreshes in both JAVA and ABAP stacks.

RSLF configuration for all production systems in solution manager

Installed XI/PI installation .

Performed Support pack upgrades.

Strong experience on Printer configuration and trouble shouting

Tech monitoring configuration for all production systems.

Manage system and Tech monitoring configuration for HANA ABAP and HANA

DB systems.

Trex installation.

Vertex monthly patch update and vertex version upgrade from 6 to 7.

Performed remote client copy, export and import, within the system and

across the systems.

Import daily and weekly transport requests at SAP level using STMS and OS

level using Transport Control Program (tp) from Development to Quality and

Production and coordinated with ABAP and Functional teams to resolve any

transport related issues.

Working with Config tool, SDM and Visual admin.

Performance Monitoring and Tuning.

Applying support patches and support packs stack on both Java and ABAP.

Work experience in Incident management in Solution manager.

Update Profile parameters and their values.

Abap dump analysis/ Background jobs management like checking the status

of background jobs, scheduling background jobs.

Monitoring R/3 system with CCMS Monitoring short dumps, system logs, db

alerts, SQL traces, Lock entries.

Project 2

Client1 : DSM, DSP

Period : April 2013 May 2015

Designation : **`SAP Basis Consultant`[EMPLOYER_ROLE]**

Client Description:

Royal DSM is a global science-based company active in health, nutrition and

materials. By connecting its unique competences in Life Sciences and Materials

Sciences DSM is driving economic prosperity, environmental progress and social

advances to create sustainable value for all stakeholders simultaneously. DSM

delivers innovative solutions that nourish, protect and improve performance in

global markets such as food and dietary supplements, personal care, feed, medical

devices, automotive, paints, electrical and electronics, life protection, alternative

energy and bio-based materials. DSMs 24,500 employees deliver annual net sales

of around 10 billion. The company is listed on NYSE Euronext.

Responsibilities

Downloading the Business component list as per steel industry requirement

and added to solution manager and generating the Stackfile, downloaded

the patches and side effect reports from time to time through solution

manager..

Applying SAP License.

Preparation of the backup calendar.

Downloading patches of download basket from SAP Download Manager

Carried out SAP Kernel Upgrade in all SAP systems includes Dialog Instances.

Configuration of Operation Modes.

User creation/deletion/maintenance, Mass User Changes.

Performed remote client copy, export and import, within the system and

across the systems.

Import daily and weekly transport requests at SAP level using STMS and OS

level using Transport Control Program (tp) from Development to Quality and

Production and coordinated with ABAP and Functional teams to resolve any

transport related issues.

Backup from Brtools, Working with Config tool, SDM and Visual admin.

Performance Monitoring and Tuning.

Applying support patches and support packs stack on both Java and ABAP.

Work experience in Incident management in Solution manager.

Update Profile parameters and their values.

Abap dump analysis/ Background jobs management like checking the status

of background jobs, scheduling background jobs.

TMS configuration for 3 system landscape.

Monitoring R/3 system with CCMS Monitoring short dumps, system logs, db

alerts, SQL traces, Lock entries.

Project 3

Client2 : Jindal Steel and Power Ltd, Raigarh

Period : Aug **`2012`[TENURE]** Nov **`2012`[TENURE]**

Designation : **`SAP Basis Consultant`[EMPLOYER_ROLE]**

Client Description:

Jindal Steel and Power Limited (JSPL) is an Indian steel and energy company based

in New Delhi, India and a division of Jindal Group conglomerate. Its annual turnover

of over US$4 billion, Jindal Steel & Power Limited (JSPL) is a part of about US$ 12

billion diversified O. P. Jindal Group. JSPL is a leading player in Steel, Power, Mining,

Oil & Gas and Infrastructure. Mr. Naveen Jindal, the youngest son of the late Shri. O

P Jindal, drives JSPL and its group companies Jindal Power Ltd, Jindal Petroleum Ltd.,

Jindal Cement Ltd. and Jindal Steel Bolivia. The company professes a belief in the

concept of self-sufficiency. The company produces steel and power through

backward integration from its own captive coal and iron-ore mines.

Responsibilities

Applying SAP License.

Taking File system Backup via Tivoli.

Preparation of the backup calendar.

Creating Solution manager Login IDs for Service Desks activities.

Downloading patches of download basket from SAP Download Manager

Carried out SAP Kernel Upgrade in all SAP systems includes Dialog Instances.

Configuration of Operation Modes.

User creation/deletion/maintenance, Mass User Changes.

Performed remote client copy, export and import, within the system and

across the systems.

Applying support patches and support packs stack on both Java and ABAP.

Work experience in Incident management in Solution manager.

Update Profile parameters and their values.

Abap dump analysis/ Background jobs management like checking the status

of background jobs, scheduling background jobs.

TMS configuration for 3 system landscape.

Monitoring R/3 system with CCMS Monitoring short dumps, system logs, db

alerts, SQL traces, Lock entries.

Client 3 : IFA (Retail)

Period : Feb 2011- Jul **`2012`[TENURE]**

Designation : **`Associate Consultant`[EMPLOYER_ROLE]**

Client Description:

The International Financial Advisors Company (IFA) is a Kuwaiti closed

shareholding company incorporated in 1974. It providesfinancial advisory services

as well as managing funds, portfolios and investments. IFA has experienced an

exceptional growth, and has been recognized as one of the most affluentfinancial

companies in Kuwait. The merit of our success is to be attributed to the several

strategic alliances with local and international entities in addition to the offering of

premium services widely attractive to high net-worth clients and institutional

investors. IFAs shares are listed on both, the Kuwait Stock Exchange (KSE) and

Dubai Financial Market.

Responsibilities

User creation/deletion/maintenance.

Mass User Changes.

Client administration (creating/copy/delete).

Handling missing authorizations and Tracing authorization checks SU53.

Role administration using profile generator PFCG.

Applying support packs, add-ons.

Installed SAP GUI front end interface

Performed Spool administration

Installed and configured different types of Printing/Output Devices and spool

administration issues.

Database Monitoring for space utilization and Operating System level space

Management

Personal Attributes:

Strong believer of team work with good coordinating skills across multi

functional teams

Can efficiently communicate with different levels of people in the Organization

Capacity to work for extended hours based on need

Quick learner and easy to grasp power

Experienced to handle client interaction and counter region team handovers

Completed BOE320 training.

Received excellence award in Atos (2014)

Received success story award in Atos (2015)

Received best performance award in IBM (2015).

Received client focusfirst award in IBM (2015).

