21 ABBEY MEAD CLOS E

DARTFORD, KENT

PHON E + (44 )7 95 76 46 4 14

DA1 5UQ UK .

E-MAI L

fem igideon@ya hoo.com

G I D E O N , O K U N A I Y A

**`EDUCATION AND QUALIFICATION`[EDUCATION_HEADING]**

**`MCSE`[EDUCATION_DEGREE]** Data Management and **`MSc`[EDUCATION_DEGREE]**

Financial

Engineering

&

Analytics

Knowledge Management -

(Microsoft Certified Solutions

**`University of Bradford`[EDUCATION_INSTITUTE]** (**`2005`[TENURE]**-**`2006`[TENURE]**)

Expert)

**`B.sc`[EDUCATION_DEGREE]** **`Computer Science`[EDUCATION_COURSE]**

**`MCSE`[EDUCATION_DEGREE]** **`Business Intelligence`[EDUCATION_COURSE]**

**`Olabisi Onabanjo University`[EDUCATION_INSTITUTE]** (**`1998`[TENURE]** **`2003`[TENURE]**)

(Microsoft Certified Solutions

Expert)

**`EXPERIENCE`[WORK_EXPERIENCE_HEADING]**

**`Agile Solution`[EMPLOYER_COMPANY_NAME]** **`Business Intelligence Developer/Consultant`[EMPLOYER_ROLE]**

March

**`2018`[TENURE]** still date.

Responsibility: Developed and implemented a robust Data model using SQL

Server Analysis Service (SSAS) and delivered reporting solution using SSRS and

Power BI.

Task and Deliverables

Development and application of **`Business Intelligence`[EDUCATION_COURSE]** Solution best practise

and standard.

Active leadership and co-facilitation of stand-ups, retrospectives, showcases,

planning session.

Working to understand the existing business environment, business needs and

eliciting

business requirement with stakeholders at all levels of seniority. Managing

end-to-end clear

traceability, maintenance, prioritisation, change control and approval

mechanism.

Resilient and proactive with strong delivery focus.

Query languages used are T-SQL, MDX, DAX.

Technology platforms are: SQL Server **`2014`[TENURE]**/**`2016`[TENURE]** and Azure, Parallel Data

Warehouse -PDW

**`Ravensbourne`[EMPLOYER_COMPANY_NAME]** **`Business Intelligence`[EDUCATION_COURSE]** Developer/Consultant

May **`2016`[TENURE]**

March **`2018`[TENURE]**.

Responsibility: Designed and implemented the business Power BI Service

platform, data modelling using SSAS Tabular and delivered reporting solution

using SSRS. Below highlight some area of tasks and deliverable.

1

Tasks and Deliverables

Phase 1

Provide reporting Solution using SSRS and Power BI technology.

Liaised with different members of staff to gather report requirements and

manage expectations.

Planned, developed and implemented reporting solution using SQL Server

Reporting Service - SSRS as the visualisation.

Designed and implemented Data warehouse and have it populated with

ETL solution.

Phase 2

Using the SSAS

Designed and implemented Cubes solution using SQL Server Analytical

Service-SSAS 2014.

Facts and dimensions table were designed in accordance with business

requirements gathered

Implemented best practice partition and aggregate to improve users

querying experience

Implemented dynamic security to restrict access to report.

Implemented users hierarchy within the dimensions to improve the users

dicing and slicing experience.

Write DAX (Data Analysis Expressions) to capture business logic

Using the Power BI

Designed and published dashboards to power BI service, mostly viewed by

senior managements

Liaised with business users to capture requirements and work alongside

for effect deliverables.

Designed and deployed reports to departmental workspace for end users

Setup and implement Power BI Live connect to SSAS Tabular through

Enterprise Gateway.

Being the Power BI subject matter expert, keep up to date with Power BI

developments and working with IT to make sure the system is optimized

and using all the functionality that is on offer.

**`BPHA`[EMPLOYER_COMPANY_NAME]** **`Business Intelligence`[EDUCATION_COURSE]** Consultant

April **`2015`[TENURE]** April

**`2016`[TENURE]**.

Responsibility: Company just acquired new CRM system with a Data

Warehouse to report, so I came on board to deliver over 120 reports from the

new data warehouse.

Tasks and Deliverables

Set expectations through the correct channels, ensure relevance of

communication across the stakeholder, and proactively support of the

business need, identifying and shaping processes as required by business.

2

Actively implement agile methodology to facilitate quality solution and

delivered within period projected.

Produce SSRS output to spec from data stored in a variety of technologies /

databases.

Work with the internal team to ensure the best functioning front-end

experience.

Ensure appropriate standards and procedures are adhered to during the

development process to support a quality deliverable.

Participate in the testing process through test review, analysis and test

witnessing.

Identify andfix performance and accessibility issues with new and existing

projects.

Consult with project leads to identify current operating procedures and to

clarify program objectives.

**`Sophos`[EMPLOYER_COMPANY_NAME]** **`Lead SSAS Consultant`[EMPLOYER_ROLE]**

Jan **`2015`[TENURE]** March **`2015`[TENURE]**

Responsibility: The company is moving from private to public liability

company, which make the daily incoming day of 1million records increase to 3

million records daily and secondly, the report would have more viewers. My

responsibility is to implement a secured cube, restricting users access globally

and improved performance.

Tasks and Deliverables

Designed and implemented dynamic security model for Enterprise Finance

Cube. The Security model is a global one restricting users by Country,

Region Department and Cost Centre manager.

Improved Processing time and query time of the legacy Enterprise cube by

implementing Usage based optimized to monitor the user query,

partitioned the cube, putting aggregations in place, redesigning of the

named queries in the DSV to use indexed table from the data source.

Implemented stored procedure, column stored index and cache plan to

improve the query time of date

Design and Implement New Cube for the finance based on new

requirement of 200million records. Defining the facts tables and

dimensions and implementing star schema for better performance.

MDX script is written to capture business logic and for security restriction.

Produced Dashboards using the Power view /Power query capturing the

business measures with DAX

Developed and implemented ETL packages that dynamically manage the

Cubes partitions apportioning different processes to the Cube partitions.

Liaised with business users to capture requirements and work alongside

3

for effect deliverables.

Trained and mentored members of the BI team for knowledge transfer and

Knowledge management. Also, progress was monitored and communicated

via Agile approach(Scrum), which help to identifies threats that could

hinder delivery dates

**`Bentham Black Consulting`[EMPLOYER_COMPANY_NAME]** **`Business Intelligence Consultant`[EMPLOYER_ROLE]**

July

**`2013`[TENURE]** Dec **`2014`[TENURE]**.

Responsibility: As a consultant, implemented reporting and analytical solutions for

a wide variety of clients Majorly ASI Adams Smith International, NIAF

Nigeria Infrastructure Advisory Facility, FMoP Nigeria Federal Ministry of

Power.

Tasks and Deliverables

Collaborated with senior managers and business analysts to identify,

clarify, and prioritize the BI requirements supporting the strategic goals of

increasing customer lifetime value.

Supported and mentored the project teams, successfully inculcating a

business-value-oriented culture with the new implemented technology-

centric.

Achieved significant improvements in the process of technical data analysis

and business analytics development centered around introducing

immediate data access and analysis across the full range of activities

involved in designing and building the data warehouse and developing and

delivering high quality analytics and reports.

Design and developed complex dashboards for management using

SharePoint.

Provided technical assistance/support to analysts, business users and

responded to inquiries regarding errors, problems, or questions with

programs/interfaces

Applied security features of SSRS/SSAS to ensure sensitive data secure.

Developed and Implemented Dashboards with Power view, Power map,

Power query and capturing the business logic with DAX

Based on business requirement, developed SQL queries with Joins and T-

SQL, Stored procedure, Views, and in the Cube/OLAP to implementing the

business rules and transformations.

Data Analysis and Designed the logical and physical model architecture for

database.

4

Convert data from Legacy system to SQL Database.

Used SSRS to create reports, customized Reports, on-demand reports.

Validating and testing of the Database data, SQL codes and application.

Used XML to pass parameter for conformity.

Developed and implemented SSIS project to populate Data warehouse and

process SSAS solution.

Lancashire NHS SSAS Developer

April 2012 July 2013.

Responsibility: To Plan, Develop and manage the cubes and support the Data

warehouse.

Tasks and Deliverables

Planned and designed OLAP Cube based on Business User requirements.

This involved the modelling of the data into Fact table and dimension

tables, having a star scheme in most case and User dimensional hierarchy

to improve users dicing experience.

Integrated legacy Finance cube with HR business system for cross section

analysis based on the business requirement.

Engage in Performance Tuning Analysis Service cubes, writing complex

MDX queries and calculations and solving Analysis Services cube design

problem.

Provide expert advice during the development phase of project and in-

depth training.

Monitors the Performance of the cubes, to optimize the operation (query

Performance and the processing time).

Implement security on the OLAP System to enforce restriction of

information.

Designed and developed Scorecard to depict the business health. Writes

MDX extensively to produce report display on the dashboard and Web part.

Produce relevant documentation including design and functional

specifications and Provide additional support to the whole development

program, including QA/Review of application and systems developments.

Support the ETL team to develop ETL packages to process the Cubes in

production environment.

Wicons Ltd

SQL BI Developer

Jan 2012 Mar

2012.

Unipart

SSRS Report Developer

Nov 2011 -

Dec 2011.

Thames Valley Housing

**`Business Intelligence`[EDUCATION_COURSE]** Developer

Dec 2010 -

5

Nov 2011.

Cubic Transportation Systems **`Business Intelligence`[EDUCATION_COURSE]** Developer

Nov 2008

Dec 2010

Pegasus Incorporation

MI Analyst

Mar **`2006`[TENURE]** Nov

2007

M B Consulting

M I Analyst

Jan 2001 April

2005

Summary of responsibilities and Tasks (from 2005 to 2012)

SSRS report writing and SSRS report support

Requirements Gathering and Liaised with key stakeholders in the business

to understand, document and confirm requirements

Debugging existing reports and applyingfixes as required.

Collaborated with end users to identify needs and opportunities for

improved data management and delivery.

Managed and supported the ongoing development and operations of the

operational data store and enterprise data warehouse.

Improved and streamlined processes regarding dataflow and data quality

to improve data accuracy, viability and value.

Developed an analytical system - OLAP (CUBE) using SSAS in which

business operations/Activities can be viewed in different dimensions and

diced accordingly to track performance and project for the future.

Develop and manage packages using SSIS technology to merge data from

disparate systems to solve business issues and performance improvement.

Working alongside the database manager to further refine and develop the

management information required

Performs maintenance and administration of the database platforms

and data protection needs as they relate to the database platforms and

solutions.

Development of MI report and developing operational model using MS

Excel

Automating complex procedure by writing Macro/VBA

Performance testing to ensure code efficiency

**`REFERENCES`[OTHERS_HEADING]**

Available upon request.

6

