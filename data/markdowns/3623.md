A Y O A R I Y I B I

Email: **`ayoariyibi@gmail.com`[EMAILS_EMAIL]** | Tel: **`07868253804`[PHONES_PHONE]**

**`P R O F I L E`[SUMMARY_HEADING]**

A test analyst with over 6 years of Software testing experience, quality assurance, test planning and test execution

Im a bright self-starter with the requisite skill set to communicate effectively at all levels of the business & technology,

underpinned by detailed and structured approach to problem solving, with good experience of best practice in software

testing. A Committed team builder and member who work to high standards with particular attention to quality output while

maintaining accuracy. I possess strong analytical skills; I am pro-active, extremely flexible and adaptable to various work

environments.

I am BPSS Cleared and currently work as an OSS/BSS Test Engineer at **`Motorola Solutions Inc`[EMPLOYER_COMPANY_NAME]**, coordinating, planning,

performing and executing test activities on the Home Office ESN Project. I am keen to join a progressive organisation

where a platform to demonstrate my skills & abilities gained over the years will be given. Im interested in a new challenge.

P R O F E S I O N A L Q U A L I F I C A T I O N S

**`MSc`[EDUCATION_DEGREE]**. **`Accounting & Finance`[EDUCATION_COURSE]**

**`BA`[EDUCATION_DEGREE]**. **`Finance Management`[EDUCATION_COURSE]**

ISEB\ISTQB Foundation Level certificate in Software Testing

SAP FI/CO, Enterprise Resource Planning and Core Components

W O R K E X P E R I E N C E

M O T O R O L A S O L U T I O N S I N C .

2 0 1 6 T I L D A T E

O S / B S L E A D T E S T E N G I N E R / A N A L Y S T

Responsibility on the project: A Home Office project focused on Migrating the Public Safety Communication system of

all emergency services within the UK to a secure Smartphone solution over a commercial network.

As the Provisioning Test Lead/expert, Im responsible for conducting Supplier requirements analysis, Writing, pair-

reviewing and executing System and SIT test cases for the OSS/BSS and Public Safety Communication Service, working

closely with the network supplier for radio/eNodeB provisioning and liaising with Ericsson and the Authority (Home

Office).

Conducting and hands-on involvement in failover testing of critical services. Support and validation of the EE/ESN

integrated systems ability to be able to move operations to back-up systems during the server failure testing. All possible

scenarios within scope were covered on both MSI supplier side and EE. I lead test calls, the first point of call for defects,

collated test results, created defect matrices & PPTs.

Test Tools Used: HP ALM Quality Centre, BMS Remedy, Sisense Reporting System, Netcool Fault Management, User

Device Management Systems, Self Service Portal, CITRIX, FORTINET Client Console, Active Directory, VM Ware Air

watch, LEX L10 PTT Device, Wireshark for Diameter and SIP trace, FTP, Data logger and a host of other test tools.

Conducting requirements analysis on the ESN Test Artifacts.

Conducting failover and load balancing tests as part of the MS Access network team

Crafting and validating subscriber CSV, provisioning subscribers on multiple profiles and templates, validating

MS Access elements following subscriber provisioning, generating and logging MVNO rated and non-rated CDRs,

writing and running SQL Queries relating to provisioning, suspend, resume, terminate, change sim for subscribers.

Maintaining production and reference sim catalogues in line with Authoritys guideline and security rules.

Capturing Wireshark and SUF traces for event troubleshooting and logging.

Conducting multiple test phases in both reference and production environments and delivering knowledge transfer.

Triggering, logging and witnessing system and integration tests between ESN and EE radio service provider.

Monitoring Netcool Alarms, running alarm reports, raising defects and producing daily test status reports.

Defining Test Cases and associated pre-requisites.

Creating, reviewing and executing associated Test Cases/scripts, logging expected results with strict requirement

driven focus using HP ALM accordance with the Authority (Home Office) guidelines.

Identifying and gathering of test data requirements and definition of test data requirements.

Demonstrating good understanding of test dependencies and updating scripts once dependencies are closed down.

Adherence to testing timelines, immediately flagging any potential blockers and ensuring requirement delivery.

Pro-active participation in Test workshops and planning sessions; always speaking up with experience based

observations on test topics including; automation, environments, test execution and issue reporting.

Involved in collaborative approach with in-house and external Programme personnel in troubleshooting and

reproducing Issues.

Providing informal support and training in test processes and to other teams on the ESN project.

Contributing to the on-going improvement of our internal test processes.

Conducting Tests using the dedicated UEs LEX L10 and Samsung for Group and One to One ESN Calls, MMS

and SMS calls, carrying out apn configuration and other ad-hoc duties.

A R Q I V A W I F I L T D

F E B 2 0 1 6 S E P

2 0 1 6

S E N I O R T E S T A N A L Y S T

ARQIVA provides and manages the WiFi services for Heathrow Airport and all other regional Airports across the UK.

Arqiva also services small tranches of Restaurants, Hotels, Underground and Street WiFi.

Responsibility on the project: Responsible for ensuring solutions delivered to stakeholders meet their business and quality

expectations. I Report to the IT Solutions Test Manager work in a small size team of testers and liaise with stakeholders.

(Test Planning / Scripting / Preliminary and Regression Testing / Test Reporting and Analysis / Device Validations)

Test Tools Used: JIRA for Test and Defect Management, Atlassian Confluence as a Wiki, Wireshark and Charles Proxy for

web debugging and analyzer tool and IntelliJ Selenium Web driver for running already written automation scripts.

Specialized in BAU and 4th Line Support projects from big and small tranches serviced by Arqiva.

Hands-on support on Service Migration projects involving servers, data centre migration and clusters.

Demonstrating sound understanding of the IT security principles and practice.

Demonstrating sound understanding of the WIFI architecture, ACS, APs, Servers and network platforms.

Conducted different levels of testing, E2E test, B2B test, Roll Back Test, 3rd Party Connectivity test and failover test.

Owned and managed the defect process in JIRA and involved in building the test request process and workflow

Analyzed the system requirements and developed test plans using technical specifications, involved in creating test

scripts for manual testing on JIRA, supported and ran automation test scripts using Selenium and JMeter.

Preparing Test Scenarios by analysing the User Stories and liaising with Business Analysts for gap analysis.

Accurately tracked and reported test progress to the management team.

Responsible for the quality of work done within the team and ensured that standards are followed at all times.

Worked with the team to identify areas for improvement and assisted in their implementation.

Worked closely with developers (TDD) in order to perform testing activities as per schedules, with the NOC on issues

relating to network, servers, monitoring and reporting, with the UX and media manager for design and documentation

Documented test plans, test scripts and text exit reports in JIRA Cloud and on the Web-based collaboration and

document management system.

Performed back-end testing, using SQL, Jason CMS Portal,

Generated test scripts for regression testing on Windows and Mac OS platforms.

Conducted cross-browser tests across Windows, Safari, Firefox and Chrome and cross-device testing across IOS,

Windows, Android and Blackberry enterprise testing devices.

Demonstrated good attention to details, and helped deliver a good number of projects successfully.

Created a How To guide on using Atlassian JIRA and Confluence and giving walkthrough to new users.

Configured and Installed plug-ins to JIRA such as GIT, Hip Chat, Timecard, Jenkins Tool, and others.

Assigned Users to projects on JIRA, configured components, generated defects and status reports in tables and graph,

linked requirements on Kanban Board to test ticket and confluence calendar plug in and other JIRA Administration.

P A R E N T P A Y L T D .

O C T 2 0 1 5 JAN 2 0 1 6

T E S T A N A L Y S T

Responsibility on the project: Testing a New Product called Nimbl. A Prepaid Youth Contactless VISA Card

(Test Planning / Scripting / Preliminary and Regression Testing / Test Reporting and Analysis / Device Validations)

Test Tools Used: Octopus Deployment and release management server to deploy the ASP.NET web application from the

integration environment to the manual and automation environments, Test Fairy to redeploy builds of the Nimbl application

to IOS, Android and Windows Platforms. SQL Management Studio 2014 extensively to reconcile updates, retrieve and

manage card account data and trace errors, TeamCity Rest API to track development tasks and web interface API Call Tests.

Version One Story Board for BDD, Test and Defect Management Tool to create user stories, raise tasks, create and track

defects, track burn-down graphs in relation to planned sprint and record executed test cases.

Preparing Test Scenarios by Analysing the User Stories and liaising with the Test Lead for gap analysis.

Writing High, Medium and Low priority test cases and pair reviewing.

Managing story defects in test & Re-Testing after the bugs have been fixed.

Regression testing using VersionOne and prioritizing test cases in readiness for release.

Using RoundCube to manage server mailbox and Maintaining the Shared Drive and SharePoint for secure storage.

Testing the full functionality of the public website with test users and inspecting the java console.

Testing the full admin website with different users and user permissions and creating new admin users.

Accepting stories on the storyboard after successful test execution and Carrying out Smoke and Regression testing.

Responsible for creating test data using excel sheets, executing test cases, logging the defects and working closely

with Development and non-technical members of the team on issue resolution and process optimization.

Performed Cross-Browser Testing to validate compatibility of the nimbl app on different OS, browsers and plug-ins.

Adding stories to backlogs in anticipation of soft launch and then implementing for Public release.

V O D A F O N E U K (Aricent Engineering)

D E C 2 0 1 4 A U G 2 0 1 5

T E S T A N A L Y S T

Responsibility on the project: Android Phones, Motorola phones Carrier validations (Scripting/Testing/TD/ Test

Centre/JIRA Defect management & Perfecto Automated system)

Test Tools Used: Motorola test trace tools, Test Centre, Radio Coms, QXDM Tool, QPST, RSDlite.

Used 2G GSM/ GPRS/ 3G / 4G Smart phones LTE Tablet/Phone devices Smartphone handset testing & Mainly Software

Application and protocol testing.

Reviewing high and low level designs, business requirement and functional specification documents.

Working on Multiple projects relating to security, mobile applications testing, providing support to offshore team and

more.

Demonstrating good knowledge of network architectures and nodes relative to LTE, IMS, VoLTE, 3G/2G E-Utran.

Understanding protocols such as Diameter, SIP and encrypted packets. General knowledge on Signalling system

protocol 7 (SS7).

Liaising with the network team to gain extensive understanding of the network nodes and their functionalities.

Applications testing to include all social networks like Yahoo, Gmail, Google and other Android Apps testing, WEB,

Browser, Stemming, Download, Email, FTP, Tethering/ Hotspot, Gmail and other applications. Mobile handset

standard testing. Server client base applications like Facebook. Social network applications And Camera web storage

and local storage Applications. Very familiar with Media, BT and WIFI technology test cases, installation of new

Apps and control test Apps and Mobile applications.

Work on Android Motorola phones software testing UMTS & Wifi Devices, Tablet and Smart devices, Software

stability and network testing. Air interface network messages network CS/PS PDP connection interface testing, good

understanding of the interfacing systems.

Mainly carried out Application test plans, writing test cases to verify requirements.

Worked in full Software test Process from the requirements, Functional requirements to Design test cases and Run the

test case/sets. Also error management process. Used Technology works on UMTS/3G/LTE Standard. Understanding

and use of resource planning process. Delivery plans covering all project on time with **`Vodafone`[EMPLOYER_COMPANY_NAME]** system.

Software integration testing, Regression testing of software releases, Configuring test equipment, handsets and

software tools. Flash handsets from development to Carrier Validation / Retail & Network operators like Orange, O2,

EE & **`Vodafone`[EMPLOYER_COMPANY_NAME]**.

Leading E2E testing from the Mediation Zone to MTAS (Core Network), analysing logs and call flows.

Working with the provisioning and VATS teams to ensure subscribers provisioning is carried out perfectly and issues

resolved and also ensuring accounting records (ACR and CDRs) are generated successfully and correctly during the

test cycles.

Performing tests on iOS, Android, Blackberry, and Windows devices and applications Including native Applications,

Market Applications and web based applications.

Managing test scripts and defects identified through the software lifecycle using HP Quality Centre tool and Jira

Creating a Test structure for new Test cases, test scripts and mapping the Requirements to the test plan.

Performing functional, non-functional and end-to-end scenario testing.

Taking SIP and Diameter traces and logs from Android, iOS, Blackberry, and Windows Devices using WireShark

Producing clear and detailed test result reports and able to communicate with external customers effectively.

Working on multi projects such as VoLTE and VoWIFI, Virtual Sim Platforms, SMS and eCare, Simplified and more

Liaising daily with stakeholders from Oracle, Ericsson and NSN for network configuration and subscriber

provisioning

Attend scrum meetings and produce daily RAG reports to test lead and customers.

Attend workshops, as required, and liaise with 3rd parties, users, programmers and other areas of the business.

Carrying out retests and maintaining regression test packs, reviewed testing and also highlighted areas for

improvement.

Carrying out preliminary and sanity tests and working closely with the production support and network support to

resolve environment issues and changes.

Demonstrating excellent interpersonal skills, supporting on different projects where required, contributing to training

sessions and sharing knowledge with new recruits.

Prioritizing test cases during planning and execution based on business priorities arising from strict deadlines and

timescales.

B A R C L A Y C A R D U K

A P R I L 2 0 1 3

N O V 2 0 1 4 T E S T A N A L Y S T

Responsibility on the project: Requirements of the business. There are at least 6 releases a year where the team is

responsible to carry out User Acceptance Testing (UAT). In doing so we continually deliver a quality internal and external

customer experience when using the CAO Platform

Preparing Test Scenarios by Analysing the Business Requirements Document, Executing Test Cases for UAT.

Managing defects & Re-Testing after the Bug has been fixed.

Regression testing using test tool.

Maintain the Share Drive and SharePoint and prepare the following:

BUT(Business Updateable Text) catalogues

Docs Specs (Logic and Layout)

Scope Tracker, Design and Delivery Tracker

COMMS & Command and Control Pack

Analysed the requirements of the application and involved in preparing Traceability Matrix and writing the test cases

Involved in Retesting and Regression testing.

Responsible for creating test data using excel sheets and executing test cases and logging the defects and working

closely with Technical and non-technical members of the team on issue resolution and process optimization.

Performed Cross-Browser Testing to check the compatibility of the application on different Operating Systems.

L O Y D S B A N K I N G R O U P

J U L Y 2 0 1 2 - M A R C H

2 0 1 3 Q U A L I T Y A S U R A N C E A N A L Y S T

Responsibility on the project: Next Generation Mobile Banking App is developed for famous Lloyds TSB group (Lloyds

Bank, BOS, and Halifax) and enables customers to make Payments & Transfers, Account statements, Spending Rewards,

Web trends and Find ATM/Branch etc via GALAXY across iOS and Android platform devices.

Responsible for Preparing Test Hierarchy from **`BA`[EDUCATION_DEGREE]**, UX and User Journey's for each sprint with high focus on quality

and good test coverage.

Responsible for preparing test cases for Mobile Banking features/Business requirements.

Responsible for running UI Performance, Web trends, Network Connectivity, Device Configuration tests.

Responsible for End to End testing for Spending Rewards feature.

Performing Smoke Test, Functional and Exploratory Testing.

Performing copy and Design testing for Lloyds, Bank of Scotland and Halifax.

Performance testing of the Application on WiFi, 3G using Charles tool etc.

Reporting and Verification of defects in JIRA by discussing with Developers, **`BA`[EDUCATION_DEGREE]** and UI Design teams.

Working closely with Developers to ensure bug fixes.

Environment: iPhone5, iPhone 4S and 4, iPod, iPad, Samsung S3, Samsung Ace, JIRA, X-code Instruments, Charles

tool, REST APIs/JSON, Wi-Fi, Jenkins, Testflight.

Analysed Business requirements documentation and other related documents to understand the applications

functionalities and walkthroughs on test documentations before sign off by stakeholders.

Created test plans, strategies, test scripts on QC to ensure appropriate test coverage for all test artefacts in project.

Organise regular project status review meetings on issues & generate issue reports for review with business users

Selected appropriate test case for test automation and also for performance/load testing.

Carried out regression testing as necessary and maintained regression test packs for application functionality.

Took delivery of release notes for defect fixes, Analysed release notes and reject if necessary.

Coordinating and monitoring of defects, Bug tracking and testing on XML application to meet user requirements.

Escalated key issues, risks to the test manager and project manager to ensure testing status progress and next steps are

regularly communicated to all key stakeholders.

Prioritised tests during planning and execution based on business priorities and risks arising from strict timescales.

Performed E2E testing on system application as well as report issues before they adversely impact the testing

H U T C H I S O N 3 G U K L T D

A U G 2 0 9

J U N 2 0 1 2

M O B I L E T E S T C O N S U L T A N T

Responsibility on the project: Providing Field Test Support for Burst Requirements through Absolute Validation for

clients H3G heading into a busy period conducting live field trials on both UMTS and LTE networks in Reading, London,

Birmingham and Manchester UK for all major UK carriers.

Conducted testing on 2G/3G mobile phone, tablet and camera devices on Android platforms

Analysed BRD to understand the applications functionalities for the Mobile web application

Conducting Voice, data call, FTP transfers, SMS both static and mobility.

Working on Windows/IOS/Android platforms and all major UK network providers.

Using Web based apps to automate testing.

Created Test Conditions from Use Case & ensure full test coverage following a Risk-based Testing approach

Re-organised and re-ordering of Use case from Business requirement and Prototype screens. Carried out retests and

maintained regression test pack, reviewed testing and also highlighted areas for improvement

Creating new starter document kits and process guides as well as test suites.

Working on improvements and efficiencies using Prince 2 methodologies.

Communicating with stakeholders globally to ensure issue resolution and reporting targets and quality levels are met.

Managed incoming inventory logging control and distribution to U.K. team following successful screening.

Contribution to daily progress and process improvement meetings.

Training new starters and helping with queries

Contributed to the implementation of test infrastructure for processes, methodologies, tools, data and environments.

T E C H N I C A L S K I L S

Operating System &

Windows 8, XP, 2000, NT,/98, Unix ,Linux,IE6,8,10,Firefox 45.0 16.0 & Safari 5, 6

Browsers

Testing Tools

HPQC, ALM, TD, TP, Mercury Quality Centre 9.2, Winrunner, BugZilla, Bug Tracker JIRA,

Version One

Methodologies

V-Model, TDD, Agile, Scrum, XP, DSDM, Iterative, Lean, RAD &Waterfall,

Database & MS Office

Access DB, TOAD, Oracle Database, SAP ERP, ECC, Ms. SQL Server Mgt. Studio & CRM,

Oracle SQL Developer, Excel, Linux, WireShark & (Pivot tables, V-Lookups, Macros),

Access &PowerPoint.

Framework, Internet Tools / JavaScript DB2 UDB, HTML/CSS/JL, HTTP, Oracle, JAVA (J2EE), XML, ASP.NET,

Languages

Mcrosoft.Net, SharePoint, ALPHA Framework PHP, MySQL Server & Oracle, Silver Stripe.

P E R S O N A L S T A T E M E N T

I have wealth of knowledge and hands on experience in E-Commerce, web and application testing, system, integration and

unit testing across telecoms, legal, infrastructure and financial services domains. I am flexible and resourceful team player

with excellent communications and interpersonal skills. I also possess client facing and mentorship skills. My experience has

been a varying combination of both hands-on and hands-off technical testing roles. Strongly customers focused with a good

appreciation of the challenges. I have got strong written & verbal skills. I have a proactive approach to problem solving, able

to troubleshoot in a creative, effective & prompt manner. I am able to assimilate & apply new technical knowledge, self-

manage multiple work demands in a professional way, exercise initiative and good judgment to take decisions. I keep a

high attention to detail ensuring quality work & services are delivered. I am able to communicate clearly at all levels of

management internally and externally, also with suppliers in a clear and concise manner. I am tact, diplomatic & able to

methodically approach technical issues clearly & simply.

R E F E R E N C E

Available on Request

