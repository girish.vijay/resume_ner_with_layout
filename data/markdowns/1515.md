**`Bengaluru`[ADDRESS]**, Karnataka

(+91) 9121470797

A YUSH S HUKLA

India. PIN - 560034

**`shukla.ayush1729@gmail.com`[EMAILS_EMAIL]**

**`E MPLOYMENT`[WORK_EXPERIENCE_HEADING]**

**`Software Development Engineer`[EMPLOYER_ROLE]**

**`slice(Formerly SlicePay)`[EMPLOYER_COMPANY_NAME]**

July **`2019`[TENURE]** - Present

Rewriting the website(sliceit.com) from scratch in React.js.

This is the website from where the customers perform all the operations.

Developed website for Quadrillion finance.

worked on AWS to integrate the client side code. .

Developed Various email templates

Implemented stable React components and stand-alone functions to be added to any future pages.

**`Fullstack Developer`[EMPLOYER_ROLE]**

**`GrabOn`[EMPLOYER_COMPANY_NAME]**

July **`2018`[TENURE]** - June **`2019`[TENURE]**

Developed an admin portal(Gift Card Admin) which is used to keep track of the sales, order, delivery, etc of gift

card. This tool is the heart of our gift card business.

Integrated User facing angular components with server side using RESTful Web services.

Applied Angular's template driven validations for client side validations and CSS for further enhancements of the

presentation layer.

User authentication through JSON Web Tokens (JWT)

Developed another admin portal for one of our projects(Decorator) which was a designing tool

The Decorator Admin provides the features to restrict and monitor the full designing and tracking of the product.

Developed an interactive User Interface using HTML, CSS and Angular 4.

Application backend implemented as Node.js Express.js application server.

User authentication through JSON Web Tokens (JWT).

Used JSON for accessing data from Angular Services to Node.js APIs.

Used git for version control.

**`Software Development Engineer, Inter`[EMPLOYER_ROLE]**n

**`GrabOn`[EMPLOYER_COMPANY_NAME]**

Nov. **`2017`[TENURE]** - July **`2018`[TENURE]**

Task was to develop a chrome extension which could be used by our inventory team to effectively upload deals on

our website.

Previously this task was done manually but now its done using this extension which had a huge positive impact on

the business.

Developed a chrome extension for customers which applies the best available coupon automatically and gives the

maximum available discount to the customers.

**`Languages and Technologies`[SKILLS_HEADING]**

Programming Languages - HTML,CSS,Javascript,C, Python (basic)

Operating System - Linux, Windows, Mac OS

Frameworks and tools - IntelliJ IDEA, Angular, React.js,Node.js

Others - GitHub, Jira

**`E DUCATION`[EDUCATION_HEADING]**

Ghaziabad, India

**`ABES Engineering College`[EDUCATION_INSTITUTE]**

July **`2014`[TENURE]** - May **`2018`[TENURE]**

**`B.Tech`[EDUCATION_DEGREE]**. in **`Information Technology`[EDUCATION_COURSE]**.

Main coursework: Data Structures, Design and analysis of Algorithms, Operating Systems, Software Engineering.

