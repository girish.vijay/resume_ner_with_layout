OM SARAVANA BAWA

KARAKAMBADI BHAVYA

D.No:10-71,Yogimallavaram(village),

Tiruchanoor(post),Tirupati, Andhra Pradesh - 517503

**`karakambadibhavya@gmail.com`[EMAILS_EMAIL]**, **`+91-9703282105`[PHONES_PHONE]**

CAREER OBJECTIVE:

To work in a professional manner where I can utilize my knowledge, skills and to develop

myself along with the improvement of organization.

ACADEMIC QUALIFICATION:

Course - Institution - Board/ University - Year of Completio n - Percentage

B. Tech (**`ECE`[EDUCATION_COURSE]**) - Sri Venkateswara college of Engineering, Tirupati. - JNTU Ananthapur - **`2017`[TENURE]** - 71.03

**`Intermediate`[EDUCATION_DEGREE]** - Murliraos Junior College, Tirupati. - State Board of Education - **`2013`[TENURE]** - 87.7

**`Secondary School`[EDUCATION_DEGREE]** - Ravindra Bharathi High School, Tirupati. - State Board of Education - **`2011`[TENURE]** - 83.67

TECHNICAL SKILLS:

Programming Languages : C, Basics of Java.

Web Technologies : HTML, CSS, JAVASCRIPT

Design Tools : MATLAB.

AREA OF SPECIALIZATION:

Web Designing

Communication systems

WORKSHOPS ATTENDED:

Attended a workshop on ROBOT OPERATING SYSTEM at Chennai.

Attended a workshop on BASIC GOLD QUALITY at Tirupati.

AWARDS AND ACHIEVEMENTS:

Awarded Silver medal in 8 th All India Child &Youth Art Exhibition-2008 at Rajahmundry.

Awarded Second place in Dharma Praveshika Organized by TTD at Tirupati.

PROJECT:

Title : Vehicle Speed Control And Accident Avoidance System Using CAN

S/W Used : MATLAB

Description : Automotive Vehicles are increasingly being equipped with collision

avoidance and warning system for predicting the potential collision with an external

object, such as another vehicle or a pedestrian. This project presents an automatic

vehicle accident detection system using GPS and GSM modems. This system can be

interconnected with the car alarm system and alert the owner on his mobile phone.

This detection and messaging system is composed of a GPS receiver,

Microcontroller and a GSM Modem. Thus the main aim is to control the speed of the

vehicle to avoid accidents.

INPLANT TRAINING:

Trained in HTML, CSS, JAVASCRIPT, COREJAVA in Naresh IT Solutions at Hyderabad.

Trained in C Language in Sharath Infotech Solutions at Tirupati.

STRENGTHS:

Confidence and Positive attitude.

Self -Motivated.

Very ease when working in team and Optimistic in Nature.

EXTRA CURRICULAR ACTIVITIES:

Member in National Service Scheme (NSS) .

Organizing for ZOSH-2k15 in SV colleges.

Won prizes in school level essay writing competitions.

PERSONAL DETAILS:

Fathers Name : K Malleswaraiah

Mothers Name : K Kusala Kumari

Date of Birth : 06-05-1996

Languages Known : English, Telugu and Tamil

Hobbies : Playing Carroms, Riding bicycle,

DECLARATION:

I, Bhavya K do hereby confirm that the information given above is true to the best of

my knowledge.

Date :

Place : Tirupati

(K BHAVYA)

