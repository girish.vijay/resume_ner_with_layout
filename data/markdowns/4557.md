S K I L S

Product Management, Business

Analysis/Consulting, Competitor analysis,

Market research

**`E X P E R I E N C E`[WORK_EXPERIENCE_HEADING]**

**`Senior Associate`[EMPLOYER_ROLE]** | **`Mindtree Ltd`[EMPLOYER_COMPANY_NAME]** | May 16-

Present

Product manager for Mindtrees Revenue Guidance

application As a part of an Agile team, worked on

creating user stories, conducted sprint planning,

and product demos.

BA for Composable Automation Platform

Engineering (CAPE) team - Analyzed competitors

and provided a value proposition and go-to-market

strategy for this platform. Also drove business

communications and campaigns along with vendor

management

Created an affinity ranking model using social

media analytics to identify operational gaps for an

Airline Co.

Worked on Customer Journey map for hospitality

industry and suggested possible digital solutions

across different phases and touchpoints based on

gathered insights

Used Digital Maturity Assessment framework to

understand the maturity level and lagged areas for

a packaging good company with respect to its

competitors

Created BRD, Use cases and requirement matrix

for digitizing the existing library system for a client

**`Market Research Intern`[EMPLOYER_ROLE]** | **`ITC Ltd`[EMPLOYER_COMPANY_NAME]** | Apr 15-Jun

15

Defined and hypothesized creative evaluation

approaches to quantitative research among children

Implemented two easy-to-understand and fun games

1

**`O B J E C T I V E`[SUMMARY_HEADING]**

An **`MBA`[EDUCATION_DEGREE]** graduate having about 3.5

years of experience with proficiency in

Business analysis/consulting and

market research. I intend to utilize my

skills to formulate effective business

strategies, which result in positive

outcomes.

aiyappa.meghana@ **`+919164171110`[PHONES_PHONE]**

gmail.com

https://

www.linkedin.com/in/

meghana-aiyappa-

b11998b4

V O L U N T E R

E X P E R I E N C E O R

L E A D E R S H I P

Core committee member of the

Corporate Social Responsibility

Club and **`Marketing`[EDUCATION_COURSE]** Club, Christ

University

Drove ad review section of the

college marketing magazine

Horizon Times

Master of Ceremony for various

corporate interface events held at

Christ University

Lead coordinator for (south zone) for

ROBO-OPUS by Robosapiens, India

(2012)

Represented and won in various

university athletic meets

for children to extract their preferences, image and

sentence association and sorting. Gained experience

in implementing Likert Scale on various attributes of

the stimuli

M A S T E R T H E S I S

The research resulted in a strategic way to capture

preference data for marketing and has been

Completed a dissertation on the topic

implemented

Factors affecting consumer buying

behavior of mass market luxury fashion **`Programmer Analyst`[EMPLOYER_ROLE]** | **`Cognizant Technology

Solutions`[EMPLOYER_COMPANY_NAME]** | Oct 12-Apr 14

across genders as a part of the

curriculum.

Worked with the WebMethods based business process

integration team to design and develop e-Business

transaction gateways for a global media giant

As a part of this, initiated and implemented internal

communications across entire ASP-BPI business unit

Led events for the project by collaborating with

designers and writers from the internal marketing

communications team

**`E D U C A T I O N`[EDUCATION_HEADING]**

**`MBA`[EDUCATION_DEGREE]** | May 16 | **`Institute of Management, Christ

University`[EDUCATION_INSTITUTE]**

Specialization: **`Marketing`[EDUCATION_COURSE]**

CGPA: 2.6/4

**`BE`[EDUCATION_DEGREE]** | Sept 12 | **`M.S Ramaiah Institute of

Technology`[EDUCATION_INSTITUTE]**

Specialization: **`Instrumentation Technology`[EDUCATION_COURSE]**

CGPA: 8.24/10

2

