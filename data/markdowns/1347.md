MRS. NANDITA MATHUR

1st Floor 168 Sector 22A near Dr. Lal Path Lab

E -Mail **`nandita05mathur@gmail.com`[EMAILS_EMAIL]**

Opposite Tikona park

Contact **`7877078732`[PHONES_PHONE]**

Gurgaon 122016

**`CARRIER OBJECTIVE`[SUMMARY_HEADING]**

Analytical and process-oriented data scientist seeks an entry level position in a successful

yet growing organization where skills such as statistics processing and presentation

development can be applied in a real-world corporate setting.

**`ACADEMIC CREDENTIALS`[EDUCATION_HEADING]**

Particulars - Institute - Passing Year - Percentage

(CBSE) - Subodh Public School, **`2011`[TENURE]** Jaipur - 76%

th **`12`[EDUCATION_DEGREE]** standard (CBSE) - Subodh School, Jaipur - Public - **`2013`[TENURE]** - 63%

**`CERTIFICATIONS`[OTHERS_HEADING]**

Certified Training in Exackt Techfleeters in Python, Data Analytics and

Machine Learning in 2019.

Certificate from Dotsquares for completing project successfully in 3 months

training period.

Certificate of participation in Infosys Foundation Program in 2017.

**`ACHEIVEMENTS`[OTHERS_HEADING]**

Won Best Employee of the month in Bell Technology, Jaipur.

Participated in various extra curriculum activities such as Dance Competitions,

Singing Competitions, Event Planning.

**`PROJECTS`[OTHERS_HEADING]**

**`B.TECH`[EDUCATION_DEGREE]** (CS):

Company Name Dotsquares, Jaipur

Project Name InfoChirag A Service Provider

Project Objective A GUI based desktop application, in which with the help of MVC

we designed a service provider portal which divided into different sections as

Mechanics, Plumber, Electrician, and Cobbler.

TRAINING:

Project 1:

Company Name Exackt Techfleeters, Jaipur

Project Name Cafe Management System

Project Objective Using Python and Tkinter, a basic cafe management system

portal in which using can login or signup using email id and order selected food

items and its quantity. After bill is generated automatically using calculator and

printed.

Project 2:

Project Name Analyze Different Datasets

Project Objective From Kaggle, there are no. of practice datasets from which we

use for enhancing our skills to predict any dataset. I worked on Black Friday, IRIS,

College Data, Titanic, Google Landmarks etc.

PRACTICE:

Project 1:

Project Name Tick-Tack-Toe Game

Project Objective Using basic python and its functions, I designed this game. In

which ask user the turn, flip turn to another user, check for win, check for game

going or over.

Project 2:

Project Name Turtle Race Game

Project Objective Firstly learn and run turtle module in python. Then, applied

different functions used in turtles like shape, speed, forward etc. Using these

functions I created this game where random color turtle starts and wins the race.

SOFTWARE AND HARDWARE SKILLSET

Programming Language

Python, HTML

**`Database Skills`[SKILLS_HEADING]**

Data Cleaning, Data Extraction, Data

Collection, Data Processing, Data Analytics,

Statistics, Data Visualization, Relational

Database Knowledge, Machine Learning

Database

MS SQL

Working Platform

Windows

Software IDE

Python IDLE, PyCharm, Anaconda

Navigator, JUPITER, MATLAB

Libraries

SciPy, NumPy, Matplotlib, Pandas, Seaborn

PROFESSIONAL TRANINGS

Pursed 6 months Training in Exackt Techfleeters in Python, Data Analytics and

Machine Learning in 2019

MY STRENGTH

Positive Attitude, Confidence and ability to withstand pressure.

Quick Learner

Versatile and easily adaptable to work on any environment.

Self-starter and motivated

**`PERSONAL DETAILS`[PERSONAL_DETAILS_HEADING]**

Spouse Name

Mr. Vinay Mathur

Date of Birth

30-**`12`[EDUCATION_DEGREE]**-1994

**`Language Known`[SKILLS_HEADING]**

English, Hindi

Current Location

Gurgaon

**`Hobbies`[OTHERS_HEADING]**

Singing, Dancing, Watching TV-Series,

Listening Music

**`DECLARATION`[OTHERS_HEADING]**

I hereby declare that the details mentioned above are true to the best of knowledge.

MRS. NANDITA MATHUR

