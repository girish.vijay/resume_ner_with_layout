**`A Appalaraju Giduthuri`[NAME]** -NL- Address: #49, Near New Cambridge Public School, Muniappashetty layout, Doddanagamangala, Electronic City Phase -2 -NL- Bangalore -560100 -NL- Mobile: **`+91-8317579631`[PHONES_PHONE]** -NL- E-Mail: **`connectaditya406@gmail.com`[EMAILS_EMAIL]** -NL- **`Experience`[WORK_EXPERIENCE_HEADING]** -NL- A passionate, driven and motivated Technology professional with 6.6 years of work experience in IT and -NL- Telecom Domains. -NL- August -2018 – Present -NL-**` DXC Technologies`[EMPLOYER_COMPANY_NAME]** -NL- Major Incident Manager -NL- Skill Set: CCNA, MS Office -NL- Project: ITIO -NL- Client: HPE, HPI and DXC -NL- Work profile____________________________________________________________________________ -NL- I am now working in the Operations Management Centre for the clients DXC, HPE, HPI & MicroFocus Company. -NL- There is no major job scope change from my previous role since i have got converted to permanent role from TEK -NL- Systems to DXC. -NL- November 2016 – August 2018 -NL- **`TEK Systems`[EMPLOYER_COMPANY_NAME]** -NL- IT Operations Manager -NL- Skill Set: CCNA, Incident Management -NL- Project: ITIO -NL- Client:**` DXC Technologies`[EMPLOYER_COMPANY_NAME]** -NL- Work profile___________________________________________________________ -NL-  Manage the Application and Site Operations. -NL-  Plan and propose the service delivery processes and ensuring successful implementation of the plan as per -NL- ITIL standard. -NL-  Interact with clients and the business to identify and analyze core business processes and workflows. -NL-  Monitoring periodically, the implementation of the planned processes and suggesting appropriate changes. -NL-  Perform incident escalation, gather info and validate business impact. -NL-  Handle Major Incidents/ High Priority Incidents and implement major incident process to get them restore as -NL- per ITIL standard within IT Infra services. -NL-  Provide ownership and responsibility for end to end Management activities for all Severity P1/P2 incidents -NL- within Global Delivery Framework. -NL-  Initiate Technical & Management Bridge calls, drive communication and coordinate with relevant teams to -NL- get the services up within service level and work with the incident teams to create and provide RCA. -NL-  Coordinate with Client and provide assurance that services will be restored with minimum business -NL- disruptions. -NL-  Engage and deal with the External Partners for quick recovery of the operation. -NL-  Ensure that incidents restoration is done within defined SLA as per ITIL standard. -NL-  Document and track the timeline of events that occurred in the process to resolution for each of the incidents. -NL-  Perform notifications and status of all incidents to internal teams and client while managing SLA's. -NL- Page 1 of 3 -NL-  Send Executive Communications and notify to senior leadership about current status & business impact to the -NL- customer. -NL-  Provide the summary and adequate updates regarding the issue to the stakeholders. -NL-  Prepare and send out the relevant reports after Conference Bridge and at the end of each incident. -NL-  After restoration, chase with problem management for RCA activities. -NL-  Coordinate with Problem and Change Management to improve the service operation. -NL- April 2015 –October 2015. -NL- **`Ikya Human Capital Solutions`[EMPLOYER_COMPANY_NAME]** -NL- L1 Engineer -NL- Skill Set: CCNA, Incident and Change Management. -NL- Project: Reliance Jio -NL- Client: Cisco Commerce India Pvt Ltd. -NL- Work profile___________________________________________________________ -NL-  Install, configure and integrate Cisco ASR 900, ASR 920 series routers to form OFC rings in CSS, AG1 and -NL- AG2 nodes. -NL-  Troubleshoot the problems related to network issues, communicate and coordinate with the associated teams -NL- to get it resolved within the SLA. Review all changes to network configuration for technical accuracy and -NL- impact. -NL-  Provide level II support for client engineers in identifying isolating and resolving network -NL- connectivity issues during leased line outages. -NL-  Provided onsite engineering, monitoring network maintenance activities and technical support to network -NL- systems, to achieve maximum network uptime. -NL-  Recommended and implemented improved documentation including troubleshooting checklists, escalation -NL- guidelines. -NL-  Strong knowledge of various routing protocols including RIP, IGRP, EIGRP, OSPF and BGP. -NL-  Good understanding of TCP protocol suite (IP, ICMP, ARP, TCP, UDP, SNMP and FTP). -NL- February 2013 – April 2015 -NL- **`Sri Venkateswara Printing Works`[EMPLOYER_COMPANY_NAME]** -NL- **`Desktop Support Engineer`[EMPLOYER_ROLE]** -NL- Work profile__________________________________________________________ -NL-  Configure antivirus software to fully protect IT environment. -NL-  Monitor the performance of the computer systems and address issues as they arise. -NL-  Provide technical support for software reconfigurations to aid in function customization. -NL-  Test software performance throughout the desktop network to ensure peak performance. -NL-  Install computer hardware and software on desktops to keep versions current. -NL-  Initiate and complete Microsoft Windows server operating system updates. -NL-  Network with LAN/WAN and active directory for continuous company connection. -NL- February 2011 – January 2013. -NL- **`Wipro Technologies Ltd`[EMPLOYER_COMPANY_NAME]**, Chennai (contract) -NL- Junior Support Administrator -NL- Skill set: -CCNA, HPSM -NL- Project: - IPS Level 2 Support -NL- Client: Capital One (U.S.A) -NL- (Technical support team) -NL- Responsible for maintenance and 24x7 Support of connectivity between different applications in production, testing -NL- and development environments hosted in the UNIX servers (HP, Linux) and the Windows servers. -NL- Work profile___________________________________________________________ -NL-  Monitor Networks to ensure the availability to specific users. -NL-  Evaluate and modify system performance. -NL- Page 2 of 3 -NL-  Maintain network facilities in individual machines, such as printers. -NL-  Analyze and isolate issues in LANs and network segments. -NL-  We perform root cause analysis or other analysis in the production environment. -NL-  Analyze the recurring issues, troubleshoot and give suggestions on how to prevent further failures. -NL-  Act as a Single Point of Contact for primary escalations in the offshore team. -NL-  Constantly work with other Teams to resolve the incidents based on Service Level Agreement, applying the -NL- Global Severity Matrix, to fix critical issues on time. -NL-  Monitor critical jobs running on the Production servers and fix bug in case of any failures. -NL-  Initiate conference calls with the clients, provide information on various issues and work in collaboration to -NL- resolve issues. -NL- **`Educational Background_________________________________________________________________`[EDUCATION_HEADING]** -NL-  **`B-Tech`[EDUCATION_DEGREE]** in **`Electronic and communication engineering`[EDUCATION_COURSE]** from **`CHAITANYA ENGINEERING COLLEGE, JNTU`[EDUCATION_INSTITUTE]**. -NL- **`Professional Qualification_________________________________________________________________`[EDUCATION_HEADING]** -NL- Cisco certified in Network Associate (CCNA) -NL- ITIL Foundation -NL- **`Personal Information____________________________________________________________________`[PERSONAL_DETAILS_HEADING]** -NL- Date of Birth: 30th June, 1988. -NL- Languages known: English, Telugu -NL- I hereby declare that all the details furnished above are true to the best of my knowledge. -NL- Place: Kakinada -NL- A APPALA RAJU GIDUTHRI -NL- Page 3 of 3 -NL- 