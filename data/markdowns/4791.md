PANKAJ VERMA

+917089889428

pan.pv31@gmail.com

https://www.linkedin.com/in/pankaj-verma-6601b4127/

PROFESSIONAL VALUE OFFERED

A High Energy Ambitious Business Development Professional with 6+

years experience in Hotel, Travels, Private public bus transport industries

handling Diverse Marketing, Development, Branding, Operations and

Territory & Area Sales for Western /Central/North Region delivering

optimal results & business value in high-growth environments seeking a

challenging role in Business Development Manager, Marketing,

Researcher, Operations and Customer Relations Management with a

world class organization and Regional Companies.

Expertise in New Development & Initiatives, corporate, Manage & Set-up

of Multiple Branches, Institutional, modern trade, Team leader and

channel sales, planning, execution and proficiency in devising and

implementing strategic plans to achieve organizational vision and

strategic direction and at establishing operational excellence within cross-

cultural environments thereby building cohesive team structures that

drive companys vision to viable realizable goals.

An excellent communicator with demonstrated capabilities in achieving

benchmarks in business enhancement and improving customer service.

Possess strong leadership, decision making, mentoring, Relationship

management skills & builder of long-term relationships while maintaining

high standards of personal performance and professionalism with ability

to relate to people at any level of business

**`KEY COMPETENCIES`[SKILLS_HEADING]**

Business Development ~ Management Process ~ Strategic

Planning ~ Organization Development ~ B2B Sales ~ Sales &

Marketing ~ Negotiation ~ Market Research & Analysis ~ P&L

Management ~ Budget & Forecasting ~ ProficiencyinExcel ~

Product & Service Concepts ~ Brand Management ~ Key Client

Retention ~ Coordination & Liaison ~ People & Team Management

~ Operations Management ~ CRM ~ Brand Marketing ~ Digital

Marketing.

**`PROFESSIONAL EXPERIENCE`[WORK_EXPERIENCE_HEADING]**

Mahendra Hotels

June 2013 - Present

(Gappu Mahendra Hotels Pvt Ltd)

Business Development Manager

Responsibility:

Managing Business Development, Marketing and Brand Management.

Managing B2B Sales and a sales team in order to maximize sales revenue.

Dynamic pricing and yield management.

Forecasting annual, quarterly and monthly sales goals.

Managing Annual, quarterly and Monthly P&L and Targets.

Coordination with the product development and IT team on the features to

be built in HMS.

Supporting and Training to the Hotel staff and team to have a thorough

understanding of the HMS or PMS.

Developing specific plans to ensure growth both long and short-term.

Educating sales and marketing team with presentations of strategies,

seminars and regular meetings.

Reviewing regional expenses and recommending improvements.

Keep a watch on competition and ensure rates and inventory with OTAs

and Offline.

Ensuring inventory, quality and prices as per the Local Market standards.

Search and identify the new Outlets in the market and Developing,

Implement, Marketing and planning of it.

Achievements:

Added Hotel and Other Outlets on board on OTAs and other online

platforms.

Developed, Monitoring and Building new Features of the HMS from 2 years

along with existing responsibility.

Trained to Hotel staff about how to use features of OTAs and other online

platform.

Started Sentiment Analysis Program.

Launched Vision, Mission and Goal of Mahendra Hotel and implements the

Policy and SOPs.

Coordination with Product development and IT team to build the HMS or

PMS with Android App.

Increased 50% Growth on OTAs and other Offline Sales.

Educated sales and marketing team and hotel staff with presentations of

strategies, seminars, regular Training and meetings.

Marketing of the products and services using digital Marketing and

Technology.

Mahendra Travels

June 2013 - Present

(Gappu Mahendra Travels Pvt Ltd)

Business Development and R&D Manager

Responsibility:

Managing Western/Central/South and North region Business Development,

Channel Management, Branding, Marketing, Research and Operations.

Managing B2B Sales and a sales team in order to maximize sales revenue.

On boarding of agents and franchisee to the company.

Relationship management with the OTAs, franchisee and agents and

customer.

Managing Annual, quarterly and Monthly P&L and Franchise & Agents

sales Targets.

Forecasting annual, quarterly and monthly sales goals.

The schedule management of the buses plying from or through the city.

Coordination with the product development and IT team on the features to

be built in ERP

Supporting and Training to the Hotel staff and team to have a thorough

understanding of the ERP software.

Dynamic pricing and yield management.

Search and identify the new Routes in the India and planning, Marketing,

Developing, and Implement of it

Participating and Being delegates in Bus awards and exhibitions of

national and International Events like; Busworld, Indiabusaward and

Prawaas.

Developing specific plans to ensure growth both long and short-term.

Achievements:

Added 200+ Buses on board on Mantis and other OTAs platform.

Trained to Agents, Franchise and staff about how to use features of Mantis

OTAs and other online platform.

Made Monopoly in some routes in Central India.

Developed the Website, App and ERP with IT team.

Setup Online Bus Ticket Booking business from scrap to 1Cr per month.

Many advance features approached and build in the platform.

Taken care of product management for 3 years along with existing

responsibility.

Involve with Directors on many big projects.

Had a very good relationship with all OTAs and vendors in the market.

Marketing of the products and services using digital Marketing and

Technology.

Developed and Implemented new technology and Features in Buses like

GPS, BHMS, ABS, Camera, Entertainments system, Wi-Fi, and Driver

Monitoring System etc.

Establish the cargo franchise of extra business.

Build the computerize reservation system with the help of local technology

guy for bus and cargo.

Connect 150+ agents and Franchise for business sharing.

**`KNOWLEDGE & SKILLS`[SKILLS_HEADING]**

Business Development/ Marketing/ Branding/ Sales:

Effectuating innovative business development plans for the purpose of

achieving pre-designated revenue plans.

Managing business development activities entailing mapping of new

clients for attainment of periodical targets.

Tracking market trends to keep abreast the changing clients

requirement and maintain a monopoly in the market.

Assessing the current market scenario and in accordance

implementing sales promotional activities and product launches for

brand building & market development.

CRM:

Customer relationships for Business Development for achieving

targets.

Enhancing relationship value of existing customers, Agents, Franchise

and OTAs by understanding and providing tailor made services.

Negotiating and Problem Solving skills and Interfacing with customers,

addressing their concerns and ensuring a high CSI.

Maintaining a good rapport with the Customers for obtaining repeat &

referral business.

Building brand value through excellent post-sales service.

Other Skills:

Proficiency in Excel, Google spread sheet, MS Office.

Strong Communication skills.

Communication and collaboration tools Knowledge (Skype, Anydesk

and teamviewer).

Social media and Cloud Computing skills.

Ability to Work under Pressure and Decision Making.

Ability to self-motivation, conflict resolution and leadership.

**`PROFESSIONAL & ACADEMIC CREDENTIALS`[EDUCATION_HEADING]**

CGBSE

10TH

2009

58.16%

CGBSE

12TH

2011

77.00 %

KALINGA

BACHELOR OF

2016

66.98%

UNIVERCITY

COMMERCE

GENIUS COMPUTER

DIPLOMA IN

2014

73.60%

CLASSES

COMPUTER

**`PERSONAL INFORMATION`[PERSONAL_DETAILS_HEADING]**

Present Address: House no 104, Block no. 5, CGHB, Kachna

Road, near railway

Crossing, Raipur 492001, Chhattisgarh.

Language: Hindi & English.

Nationality: Indian

