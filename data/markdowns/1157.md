Saket Pitamber Jha

B-304 Kanchan Chayya Appt Mahim Road Palghar(W)-401404

Cell: (+91)8007216587 Email: **`jha.saket350@gmail.com`[EMAILS_EMAIL]**

OBJECTIVES:

To succeed in an environment of growth and excellence and earn a job which provides me job satisfaction and self-

development and help me to achieve personal as well as organizational goals.

EXPERIENCE:

3 years 6 months of experience on Android application development.

Currently working in Insolutions Global Pvt Ltd from last 3 years 6 months.

Clear understanding of Object Oriented Programming, design patterns.

Have been responsible for analysis, design, development, testing and deployment.

Experience on client interaction

Worked on Material design, animations, location based services, Google maps, Google places, FCM.

Good knowledge of JSON parsing.

Having knowledge of third party libraries like Butterknife, Retrofit, GSON, Okhttp, Priority Jobqueue,

Eventbus, Calligraphy, Picasso, Universal Image Loader, Glide, Zxing etc.

Having Knowledge of Google Analytics,Crashlytics.

Worked on Android MVVM Architecture,Android Jetpack Navigation component.

Experience on payment gateway integration.

Worked on Various Security Aspects in Android App.

Version Control System -SVN

PROGRAMMING SKILLS, SDK AND FRAMEWORK:

Languages

Core Java, XML

Database

SQLite

SDK

Android SDK

Operating system

Windows 8

Debug tool

DDMS

Build tool

Gradle

IDE

Android Studio

CERTIFICATIONS/RECOGNITIONS:

I have attended the Android Developer for Beginner Workshop at Ramnarain Ruia College Matunga,Mumbai.

1

EDUCATION:

Examination - Board/ University - College/ School - Year of Passing - %/C.G.P.I

**`B.E`[EDUCATION_DEGREE]**(**`Computer Science`[EDUCATION_COURSE]**) - Mumbai University - **`SJCET,Palghar`[EDUCATION_INSTITUTE]** - **`2016`[TENURE]** - 7.75 C.G.P.I

**`H.S.C`[EDUCATION_DEGREE]** - Maharashtra Board - **`Viva,Virar`[EDUCATION_INSTITUTE]** - **`2012`[TENURE]** - 67.67%

**`S.S.C`[EDUCATION_DEGREE]** - Maharashtra Board - **`TSEHS,Palghar`[EDUCATION_INSTITUTE]** - **`2010`[TENURE]** - 84.73%

KEY PROJECTS:

1. Empower Android Application

Sponsored by Insolutions Global Ltd

Description Help Desk for ISG Customers for Project status/Ticketing.HRMS App for Employees.

Empower Instant App for easy access.

Empower offers you,

1. Employee Diary.

2. Apply Leave.

3. Attendance.

4. Leave Status.

5. Ticket Registration.

6. View Ticket Status.

7. Login using Fingerprint.

Role

UI designer and developer

Period

3 months

Team Size

2

Play store link https://play.google.com/store/apps/details?id=com.isg.empower&hl=en

2

2. Sarvalabh DMT Android Application

Sponsored by Sarvalabh

Description Sarvalabh DMT offers easy and immediate transfer of money to any Bank Account across India.

Sarvalabh offers you,

1. Remitter Registration.

2. Adding Beneficiary.

3. Transfer money to Beneficiary.

4. Transaction History.

5. Passbook.

6. Topup.

7. Profit Analyser.

Role

UI designer and developer

Period

4 months

Team Size

6

Website

https://sarvalabh.com

3. HDFC Bank SmartHub Android Application

Sponsored by HDFC bank

Description HDFC Bank SmartHub offers a wide range of payment solutions to help every business grow and

accept cashless payments from their customers.

SmartHub offers you,

1. Bharat QR Code Based Payments - Customers can scan this QR code with their phone and pay

instantly using their payment app. Merchant gets an instant notification on the status of the

transaction.

2. UPI (Unified Payment Interface) - Send UPI payment request to customers by entering

customer's Virtual Payment Address and collect money

3. Aadhar Pay - Enable Customer to Pay to Merchant using Customer Aadhar Number.For

Transaction to be successfull Customer Aadhar number should be linked with Respective Banks.

4. Passbook - Enable Merchant To Access All Transaction done by Customer and also Process

Refund.

5. Sales Summary - Enable Merchant to Access All Sales done for Selected dates.

6.Offers-Enable Merchant to access all offers notifications.

7.Pay Later Enable Customer to make payment using Pay Later module.

8.Language Added 6 different language in app.Merchant can change language from settings

module.

9.Rewritten modules using Android Jetpack Navigation and MVVM Architecture.

Role

UI designer and developer

Period

9 months

Team Size

6

Play store link https://play.google.com/store/apps/details?id=com.ionicframework.payzaap483385&hl=en

3

4. Consumer Connect Android Application

Sponsored by

Insolutions Global Ltd

Description

Consumer Connect powered by ISGPay is an integrated Platform for Providing E-Filing and

E-Payments to the Consumer.

Consumer Connect offers you,

1. The App is made for Consumer Phase Only.

2. Consumer can Access All Case Registered.

3. Consumer can Access his Advocate Hired by him and Result of Case As per his Hearing

date.

4. Consumer Can also See his Payment Receipt Available for Particular Case.

5. Consumer can Access His Respective Hearings done in Court for Particular Case.

6. Consumer can view his Profile And Contact Us for Support.

7. Consumer can Track Case Instantly without Login.

8. Consumer can search for Court Address nearby and view it in Google Maps.

Role

UI designer and developer

Period

3 months

Team Size

3

Play store link

https://play.google.com/store/apps/details?id=com.isg.consumerconnect

5.Punjab National Bank Genie Android Application

Sponsored Punjab National Bank

by

Description The app is full of features and will act as single point destination for cardholders to operate their PNB

Credit Card seamlessly and without hassles. Just download this app and log in using your PNB Card

Online User Id & Password to manage your card account on your fingertips.

PNB Genie App offers you,

1.View Past Statement.

2.Details of latest unbilled transaction.

3.Payment History.

4.Scan and Pay(Bharat QR).

5.View Bharat QR Transaction.

6.Auto Debit Feature.

7.Card Limit Increase Feature.

4

8.Pin Generation.

9.Access Credit Card Profile and Account Summary.

Role

UI designer and developer

Period

2 months

Team Size 2

Play store https://play.google.com/store/apps/details?id=com.isg.mobile.creditcard.pnb&hl=en

link

6.Epashuhaat AI Android Application

Sponsored by

Epashuhaat

Description

It is a government based application for survey of epashuhaat. It is an offline app, it syncs

data when network is available.

Role

UI designer and developer

Period

2 months

Team Size

2

Play store link

Not uploaded

7. Indian Overseas Bank Merchant Android Application

Sponsored by

Indian Overseas Bank

Description

Indian Overseas Bank offers a wide range of payment solutions to help every business

grow and accept cashless payments from their customers.

Indian Overseas Bank Merchant App offers you,

1. App allows you to Pay to Merchant by Scanning Respective Merchant Bharat QR Code.It

allow Merchant to generate Dynamic QR based on Amount to be Paid by Customer.

2. Merchant can Also see All Transaction Done by Customer using Passbook Feature

Available in App.

3. Merchant can Also Track his All Sales Done For Selected dates.

4. Merchant can Generate Dynamic QR of particular Amount to be Paid by Customer.

5) Merchant can Also select his Navigation Drawer Preference based on whether The

Merchant is Left/Right Handed.

Role

UI designer and developer

Period

3 months

Team Size

4

Play store link

Not uploaded

5

8. Currency Detection Android App

Sponsored by

Insolutions Global Ltd

Description

Currency Detection Android App is an AI based INR currency note recognizing for Visually

Impaired.

Role

UI designer and developer

Period

1 month

Team Size

4

Play Store Link

Not Uploaded

9. QRS by NPKS Android App

Sponsored by

Insolutions Global Ltd

Description

QRS Android App is an Quick response system for emergency in Hospital.

QRS App offers you,

1.Slider Shows all info regarding app use and details regarding QRS.

2.Registration Hospital/Clinic Registration.

3.Profile View Hospital/Clinic/QRS Members Profile.

4.Hospital/Clinic Users Hospital/Clinic Admin can add hospital/clinic users.

5.Alert Generation,Alert Acceptance,Alert Close.

6.Alert History.

7.Alert Dashboard.

Role

UI designer and developer

Period

3 month

Team Size

4

Play Store Link

https://play.google.com/store/apps/details?id=com.isg.ciilm

PERSONAL INFORMATION:

Permanent Address

B-304 Kanchan Chayya Appt Mahim Road Palghar(W)-401404

Gender

Male

Marital Status

Single

**`Language known`[SKILLS_HEADING]**

English, Marathi and Hindi

**`Hobbies`[OTHERS_HEADING]**

Playing Cricket and Watching Movies.

6

