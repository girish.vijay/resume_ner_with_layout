Flat No B204, Swiss County,

Thergaon, **`Pune`[ADDRESS]**, Maharashtra,India-411033

**`mayurizawar17@gmail.com`[EMAILS_EMAIL]**

+91-8805580355

mayurizawar

5 Years

2 times won Hall Of Fame award, Best Team

player.

Received appreciations from clients many

times.

**`EDUCATION`[EDUCATION_HEADING]**

**`B.E`[EDUCATION_DEGREE]** **`Computers`[EDUCATION_COURSE]** **`Pune`[ADDRESS]** University

**`2008`[TENURE]** **`2012`[TENURE]**

Score - 64 %

**`HSC`[EDUCATION_DEGREE]** Maharashtra State Board

**`2008`[TENURE]**

**`SSC`[EDUCATION_DEGREE]** Maharashtra Board

**`2006`[TENURE]**

**`SKILLS & TOOLS`[SKILLS_HEADING]**

Java

Spring, Spring boot

Microservices

Rest Api

Cloud Foundry

Maven

Hibernate

Jsp

XML, JSON

JavaScript

Oracle, Mysql

Cassandra

Toad, Sql developer

JIRA, Servicenow

Sts, Eclipse

CF CLI

Windows, Linux

Svn, Git

**`EXPERIENCE`[WORK_EXPERIENCE_HEADING]**

Company name: **`Atos Global`[EMPLOYER_COMPANY_NAME]**,**`Pune`[ADDRESS]**

Project Name: Atos Cloud Foundry (Canopy)

Client: Siemens

Period: March 01,**`2017`[TENURE]** till date

Environment: Cloud Foundry(PAAS), IAAS (AWS, TIA ,

AZURE), Java, Spring boot, Microservices, Json, SNOW,

Cassandra

Description: Atos Cloud foundry is a multi cloud

application platform (Paas) where developers can build,

deploy, run and scale their applications.

Responsibilities: Develop and maintain various

functionalities to enhance the platform (ACF) like spring

cloud, integration of ACF with servicenow etc.

Project Name: Ahold

Client: Ahold Delhaize (Netherlands)

Period: Jan 2016 till Jan 2017

Environment: Java, Spring, Struts, JSP , Oracle ,

GX(content editor)

Description: Ahold Delhaize is an international food retail

group, operating supermarkets and e-commerce

businesses. Atos used to support some of the legacy

applications of ahold.

Score 87.33 % Responsibilities: Develop and provide support for few

java based applications.

Score 84.66 % Company name: **`ADP Pvt Ltd`[EMPLOYER_COMPANY_NAME]**,**`Pune`[ADDRESS]**

Project Name: Workscape(Benefits Marketplace)

Client: CEVA, ORNL

Period: Aug **`2012`[TENURE]** Dec 2015

Environment: Java, xml , Javascript, spring,hibernate

Description: OBA (Outsourced benefits administration) is

a software to provide benefits solutions(insurances) to

the employers in US.

Responsibilities: Configure OBA and develop output files

as per client requirements.

INTERESTS

