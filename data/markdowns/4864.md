S O U R A V R O Y

87/4, Brahmo Samaj Road, Behala

**`Kolkata`[ADDRESS]**-700034

Email : **`sourav.roy525@gmail.com`[EMAILS_EMAIL]**

Mobile : **`9477645291`[PHONES_PHONE]**

PERSONAL STATEMENT:

Dynamic, passionate and imaginative.

To make use of my extensive knowledge and skills and synergize with the organization to

focus on, and achieve, the long term goals that have been put into place.

**`SKILLS SET :`[SKILLS_HEADING]**

Shell programming, Programming skill in C, C++, JAVA, PHP, Dot Net Framework,

WCF, WPF, AJAX, JQuery, VB, HTML, and CSS.

Web Page development, Good Knowledge in SQL and RDBMS.

Strong analytical, problem solving, organizational ability.

Excellent interpersonal and communication skill, decision making ability.

A team player, leadership, continuous learner.

Tenacious, independent and willing to take initiative.

TRAINING DETAILS :

Date 16th January 2015 to 16th July **`2015`[TENURE]** - Topic Microsoft Training Program, Apriso Flexnet Technology - Employer/Institution **`ITC Infotech India Ltd`[EMPLOYER_COMPANY_NAME]**, Bangalore

July 2014 to August 2014 - IBM MAINFRAME - IBM India

Date - Project Topic - Role - Employer/Institution - Language Used

Jan 8, **`2012`[TENURE]** to April 18, **`2012`[TENURE]** - ONLINE BIDDING AND PURCHASING SYSTEM - Project **`Developer`[EMPLOYER_ROLE]** - Major project in **`Scottish Church College`[EDUCATION_INSTITUTE]** under Calcutta University - MYSQL HTML JAVASCRIPT CSS

Aug, 2014 to Oct, 2014 - OMR SHEET RECOGNITION AND EVALUATION - Project **`Developer`[EMPLOYER_ROLE]** - **`MCA`[EDUCATION_DEGREE]** 4TH SEM MINOR PROJECT IN RCC INSTITUTE OF INFORMATION TECHNOLOGY under WBUT - MATLAB

Mar ,**`2015`[TENURE]** to April, **`2015`[TENURE]** - ONLINE LIBRARY MANAGEMENT SYSTEM USING WCF SERVICES - Project **`Developer`[EMPLOYER_ROLE]** - ITC INFOTECH INDIA LTD, BANGALORE - DOT NET WCF AJAX JQUERY HTML CSS SQL,LINQ

**`ACADEMIC QUALIFICATION :`[EDUCATION_HEADING]**

Name of the exam - Board - Name of the Institution - Year of passing - Subject - Aggregate

**`MCA`[EDUCATION_DEGREE]** - WBUT - **`RCC Institute Of Information Technology`[EDUCATION_INSTITUTE]** - **`2015`[TENURE]** - Computer Applications - 8.77 ( YGPA Till 5th SEM )

**`B.Sc`[EDUCATION_DEGREE]**(HONS) - Calcutta University - **`Scottish Church College`[EDUCATION_INSTITUTE]** - **`2012`[TENURE]** - Computer Science (HONOURS) - 66.875

**`HS`[EDUCATION_DEGREE]** - WBCHSE - **`New Alipore Multipurpose School`[EDUCATION_INSTITUTE]** - **`2009`[TENURE]** - Ben, Eng, Maths, Phys, Chem, Comp Sc, ENVS - 69.42

**`Madhyamik`[EDUCATION_DEGREE]** - WBBSE - **`Behala Aryya Vidyamandir`[EDUCATION_INSTITUTE]** - **`2007`[TENURE]** - Ben, Eng, Maths, PSC, LSC, Hist, Geo, Comp App - 84

HOBBIES INTERESTS AND ACHIEVEMENTS : Drawing, Programming, Scouting

:

Scouting - Rajya Puraskar Scout for Bharat Scouts And

Guides.

Participated in seminar on Application of Cryptography

in Modern Computing organized by Indian Statistical

Institute.

Participated in Entrepreneurship Development Program

at RCC Institute of Information Technology.

Had an active member of placement committee at RCC

Institute of Information Technology.

VOLUNTEERING : Worked for 8th Behala Sabuj Scouts and Guides group under Bharat

Scouts and Guides.

**`PERSONAL PROFILE :`[PERSONAL_DETAILS_HEADING]**

DATE OF BIRTH : 18/11/1991

CASTE : General

NATIONALITY : Indian

Fluent in written and spoken English, Bengali, Hindi.

**`REFERENCES :`[OTHERS_HEADING]**

Saikat Chakrabarty

Software Engineer, Polaris Networks Inc.

Subir Chakraborty

Software **`Developer`[EMPLOYER_ROLE]**, LabVantage Solutions.

Prof. Manas Pal

Assistant professor, RCC Institute Of Information technology

