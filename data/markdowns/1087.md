PPRREERRN NA A K KA ASSHHYYAAPP

: **`+91-8700927545`[PHONES_PHONE]**; : **`prrnkashyap@gmail.com`[EMAILS_EMAIL]**

LinkedIn : https://www.linkedin.com/in/prerna-Kashyap-52904992/

SSEEN NIIOORR QAQ A EEN NGGIINNEEEERR

Seeking a challenging position in Software Quality Assurance/ Testing where I can utilize my programming, critical

evaluation and debugging skills to enhance overall software quality. To be the quality specialist in the best of organization

where I can optimally use all my skills, experiences and also where I may continuously learn and work on latest technologies.

PPRRO OFFEESSSSIIOONNAALL SSU UMMMMA ARRYY:: --

5 years of experience in IT Industry with full system development lifecycle experience, implementing test

plans, test cases and test process.

Experience in mobile application testing developed for Android, iPhone as well as web based and desktop

applications.

Having hands on experience with Manual & Automation testing of Native Android & iOS app.

Having good knowledge of Android & iOS Ad SDK integration on Application.

Working on different Ad types (Native, Install ,Leadgen ,Video ,Audio ,Audio+Banner ,Carousel ,

Google Adx, Facebook Ads) on Mobile platform.

Working with Security and Performance testing of Andrioid App using Drozer agent and Trepn

respectively.

Good knowledge of Charles and Fiddler Proxy

Extensive use of Map Local, Map Remote, Rewrite tools of Charles proxy for all kind of request/response

modification.

Proficient in Selenium WebDriver, Appium with TestNG.

Have good experience in Tools Selenium, Bugzilla, TestLink.

API testing using REST client and Postman.

Experience on GIT repository for code commit and checkout.

Using JIRA for Sprint Management, Project management, Bug tracking and CCR deployment.

Proficient in creating automation scripts using Selenium WebDriver, frameworks like TestNG.

Selenium Script build and scheduling using Jenkins Server.

Proficient in creating generic frameworks and developed excel based data driven automation suites.

Proficient in bug reporting, creating and testing defects.

Hands on experience on database MySQL.

Good interpersonal skills, commitment, result oriented, hard working with a quest and zeal to learn new

technologies.

TTEECCH HNNIICCAALL SSKKIILLLLSS:: --

1. Testing Tools /IDE : Selenium 2.0, Appium

2. Test Management/ Defect Tracking Tools: Assembla, TestLink, Bugzilla, JIRA

3. Web Technologies : HTML, CSS.

4. RDBMS : MySQL

5. Languages : Java.

6. Proxy Tools : Charles, Fiddler

6. Tools : Eclipse IDE, POSTMAN, REST Client

PPRRO OFFEESSSSIIOONNAALL EEXXPPEERRIIEENNCCEE:: --

**`TIMES INTERNET LIMITED`[EMPLOYER_COMPANY_NAME]**.

**`Mobile SDK QA Automation Engineer`[EMPLOYER_ROLE]**

May **`2017`[TENURE]** Present

Developed Data driven testing Framework using Appium, Selenium Webdriver, TestNg and implemented the

same.

Creation of Automation scripts and execution using Selenium WebDriver.

Working on Charles Proxy server and extensively use Map Local, Map Remote, Rewrite tools for

request/Response modification also debugging HTTPS requests. Also creating Fiddler Script.

Working on different type (FAN, Adx, and Colombia) of SDKs integration with Apps.

Automation of Ad SDK using Appium, TestNG.

Achievements

Received Aspire award for month

Received Best Team award

Single handedly got all SDK integrations executed on All publishers apps

**`EVS`[EMPLOYER_COMPANY_NAME]**, GURGAON

Nov **`2014`[TENURE]** June **`2017`[TENURE]**

**`Quality Analyst`[EMPLOYER_ROLE]**

Involved in the preparation of test cases, communicating with software developers on quality issues following

it with bug reports, developing, running and maintaining test cases.

Performed GUI Testing, acceptance, system, positive, negative transactions and regression testing for manual

Defect tracking and reporting by Assembla.

Preparing scenarios for the regression testing.

Testing of various kinds of interfaces depending on the applications.

ACCAA ADDEEMMIICCSS::--

Passed **`B.Tech`[EDUCATION_DEGREE]** from **`MDU`[EDUCATION_INSTITUTE]**.

Passed **`12th`[EDUCATION_DEGREE]** from CBSE Board Delhi.

Passed **`10th`[EDUCATION_DEGREE]** from CBSE Board Delhi.

**`PPEERRSSO ONNAALL D DEETTA AIILLSS`[SKILLS_HEADING]**

Fathers Name :

Mr. Ashok

Mothers Name :

Mrs. Anita

Date of Birth : 13th May 1992

Marital Status :

Single

Prerna Kashyap

