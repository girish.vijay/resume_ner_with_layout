Big Data Consultant

**`bheembigdata@gmail.com`[EMAILS_EMAIL]**

**`+44 (0) 2030048549`[PHONES_PHONE]**

**`Personal Summary`[PERSONAL_DETAILS_HEADING]**

I am **`MBA`[EDUCATION_DEGREE]** qualified with Computer Science background and more than 11 years experience. I have good

experience on Data Analysis and Solution Designing on Big Data using Cloudera Hadoop, HDFS

Ecosystem, worked proficiently on Core Java, Scala, Spark, R,Real time analysis, statistical modelling,

Statistical programming, Cloudera Hadoop 5.7, HDFS, Yarn, AWS, Machine Learning, S3, Redshift,

EC2, Spark, SparkML, SparkR, RDD , Mapreduce, JMS, Storm, Kafka, Flume, Oozie, Zookeeper,

Hbase, PIG, Hive, UDF, MongoDB, SQL Scripting, SOAP, REST, Data mining, data Analysis, Oracle 11g,

Tableau 8.2, QlikView, RDBMS, OLTP, OLAP Cube, Talend, ETL Development, T-SQL & PL/SQL, VBA

Macro, Pivot, C, C++, SOAP, JSON, Performance Tuning,ACTOR BASED PROGRAMMING using

AKKA toolkit. Strong knowledge in developing REST services using SPRAY framework for cloud platform,

managing and leading team, strategy Analysis, Project monitoring, Data architecture, Data Design, data

warehousing and business intelligence, Unix AIX Shell scripting, XML, HTML, Oracle Data Integration,

GIThub Git. Have worked for clients like ABN Amro BankNV, Barclays and Nordia. Retail banking,

financial services, Trading platform environment, Capital Market, BT Network Management and Billing

system. Proficient on SDLC, Agile, ITIL, Team Leading, database design using Data modelling

methodology.

Areas of Knowledge

Big Data Development and Analysis

Software Development

System & Business Analysis

Database Development and Architecture

Network Management Systems

Advanced computer fluency

**`Key Skills And Competencies`[SKILLS_HEADING]**

Language: JAVA, PL/SQL,SQL, C, C++, VB6

Java : Core Java, JEE

Big Data: Apache, Hadoop, HDFS, Spark, Scala, Akka, Spry, Heroku, Kafka, Zoo Keeper,

Map Reduce, Hive, Pig, Hbase, Sqoop, MongoDB

DW : Informatica, Talend, Oracle Workflow, SQL Loader

BI Analytics : Qlick View, Microstrategy 8i & 9i , VBA on excel Macro & Access, R, Advance

SAS, Crystal

Report, Oracle Publisher, Reports, Oracle Discoverer

Version Control Systems: GIT, SVN, CVS, Cruisecontrol, Starteam.

**`Work Experience`[WORK_EXPERIENCE_HEADING]**

Jan **`2017`[TENURE]**-Dec **`2017`[TENURE]**

**`Inter Public Group`[EMPLOYER_COMPANY_NAME]**

**`Big Data and Clod Global Manager`[EMPLOYER_ROLE]**

Working on Media and digitalization projects for one of the top media group in world. Working on Big Data

solutions with AWS, Redshift, EC2, EMR, Hadoop, Spark, Hive, SSIS, Tableau, R, Kafka, Scala and

Java. Mainly working on architecture and development planning for data management, Data Modelling,

Enterprise Data Management, Master Data Management, Real time data analysis and insight analysis for

marketing, Sales and product development.

Jun **`2015`[TENURE]** Dec **`2016`[TENURE]**

**`Hopscotch.in`[EMPLOYER_COMPANY_NAME]**

**`Big Data Lead`[EMPLOYER_ROLE]**

Worked as Big Data head and Developing, Big Data solution for Analysis, modelling for Online

Ecommerce Business on Cloudera Hadoop 5.7, Cloud, AWS, EC2, S3, Spark, SparkR, SparkML, Scala,

Kafka, Akka , Actor, Spray Framework, Machine Learning , Talend, ETL , Redshift, Core Java, J2E ,

Hadoop HDFS , Storm, Kafka, Zoo Keeper, Docker, Mapreduce, Hive, PIG scripting, Sqoop, R,

MongoDB, SQL, Unix and QlikView for BI. developing algorithms on predictive modelling on customer

behaviour, real time analysis, Sentimental analysis based on social network, forecasting based algorithm

development. Developing Analysison action log data, Following Agile process by using Scrum and Jira,

Researching on solution for real time streaming using flume, Kafka, Spark streaming with Scala

programming.

Projects:-Ecommerce Business for Baby , Mother and households products in India, developing and

Building Big data environment for understand the customer behaviour for Marketing segmentation, Data

transaction from Face Book, Twitter, Social sentimental Analysis, Merchandising, Market trending,

Predictive Analysis. Interacting with business user to understand requirements, create user stories and

provide solutions.

Mar **`2013`[TENURE]** Nov **`2014`[TENURE]**

**`Accenture`[EMPLOYER_COMPANY_NAME]** (UK, London)

**`Big Data Analyst`[EMPLOYER_ROLE]**

Worked for one of the top Swiss Bank for their Trading Risk Management System and Capital Market

project. We were working for developing algo trading system based on trades analysis, Router market

data and twitter trend, MI reporting, developing and managing Trade capture using Calypso, Murex, WSS,

Infinity etc. we were also working on trade surveillance, P&L management and reporting processing

system. We have to develop solutions for Credit risk management and analysis, data analysis and Data

Modelling, Capture trading data, Machine Data, Twitter Data, Thomason Reuter Rics and Tips, Action log

for Analysis, development on Cloudera Hadoop 4 and 5, HDFS, YARN, NoSQL, AWS, S3, Flume,JMS,

Mapreduce, Spark, Scala, Akka , Spray, Kafka, Storm, Hive, Pig, Sqoop, MongoDB, MySQL, Oracle 11g

Data Manipulations, Performance Tuning, SQL scripting, R, Tableau Analytics, OLAP Cubes, Talend,

ETL development , VBA Macro, Pivot,Core Java, JEE Development for Data Processing and UDFs,File

transformation from Text and Jason to Avro, performance Tuning. Work with the Business user, Business

Analyst, gathering information, writing Technical Documentations, user stories.

Projects:- We learned Hadoop ecosystem and developed trading analysis system. We were working on

client side to work on Trade surveillance, developing algo trading platform, risk analysis on equity

investment, sentimental analysis using social media like Twitter and Reuter Rics, Profit and loss analysis,

worked on Cloud system AWS, worked on underwriting process. Use trade level data and static data

analysis, It allowed capturing instruments and trading level data using GUI to be monitored and managed.

Worked with business users, gathering requirements, analysis dashboards.

Nov 11 Feb 13

**`Saga Services Ltd`[EMPLOYER_COMPANY_NAME]**, Kent (UK)

**`System Consultant/Data Analyst`[EMPLOYER_ROLE]**

Duties:-

Worked on the Insurance policy administration and claim management solutions.

Analyse the functional requirement from Business, implementation and impact analysis on

the current system. Project Management activities for whole SDLC process.

Worked on Core Java, JEE development for Server side, Apache server, ANT, Oracle 11G

Data modelling, ERWIN, OLAP Cube, R, Data Analysis, Core Java, JEE, JMS, MongoDB,

PL/SQL development, Data Manipulations, Performance Tuning, strategy analysis, QlikView

Desktop and Web report development, Base SAS programming, VBA excel Macro

programming for data analysis, Pivot, MI reporting, Database Designing, Leading Customer

process. Oracle GUI development on Oracle Forms, SQL Loader, Workflow, ETL logic

development, HP-UNIX,UNIX AIX shell scripting, XML, and SOAP, XML, UT specs, Technical

documentation, Requirement gathering, suggesting the changes for improvements.

Projects:- Insurance management system, Understanding General Insurance process, Policy data

gathering and sending analytical reports to the Insurance partner companies. Claims analysis,

Underwriting rule development, working with Actuaries to understand business rules.

After Sep **`2009`[TENURE]** I took a break for Full time Master studies.

Sep 07 Sep 09

**`Atos Origin India Pvt. Ltd`[EMPLOYER_COMPANY_NAME]** (India)

**`System Analyst (senior software engineer)`[EMPLOYER_ROLE]**

Duties:

Analyse the functional requirement from client, implementation and impact analysis on the current

system. Team Leading, Project Management activities for whole SDLC using Agile methodology with ITIL

process for ABN Amro. Capturing consumer transaction, Data modelling on consumer data, transactions

of data in Data warehousing, Data modelling, OLAP Cube on consumer behaviour, Data analysis and

product management and purchase management of the retailer. Worked excessively on SQL, VBA

macros, Pivot, Base SAS, ETL development, Unix shell scripting, Informatica, Microstrategy 8i,

Microstrategy Desktop, Dashboard, MicroStrategy Office, Object manager, Intelligent Cubes, Intelligence

Server, Report Services, Enterprise Reporting, OLAP provider and Data Mart.

Projects:- worked for retail banking payment system for ABN Amro N.V..It was Retail banking Payment

based Developed and consulting analytics surrounding PD and LGD models, stress testing, and

forecasting. Data modelling, transformation, Data loading, Database designing XML, SOAP, WSDL and

Oracle , OLTP, OLAP .

Jul 06 to Sep 07

**`Techmahindra Ltd, India (Contract from Triumph Sys)`[EMPLOYER_COMPANY_NAME]**

**`Technical Associate`[EMPLOYER_ROLE]**

Duties:

British telecom projects in Techmahindra Ltd. Network management system (Complex network

configuration system) and Geneva Billing system. (Development, Database application Support &

Analysis, performance Tuning)

Core Java and JEE server side Development, Unix shell scripting for Pro C, Java script, Core

Java, JEE, XML, SOAP, Database analysis , VBA macro development, data manipulation, SQL

Scripting, SAS Data analysis and Manipulations for telecom billings, ETL transformation and

development , modelling & database development on Oracle 10g PL/SQL, Developing

performance tuning. Following and started using Agile methodology.

Jun 03 Jan 06

**`Apex Solutions Pvt. Ltd`[EMPLOYER_COMPANY_NAME]**, India

**`Software Developer`[EMPLOYER_ROLE]**

Duties:

Developed application for Sanwal Corporation.

Programming and development work on Transport Management System based on Oracle

PL/SQL 8i, 9i, SQL, Core Java programming, VB 6 and Crystal Report 7

**`Qualifications`[EDUCATION_HEADING]**

**`University of Sunderland UK`[EDUCATION_INSTITUTE]**: **`MBA`[EDUCATION_DEGREE]**, Merit (**`2011`[TENURE]**)

Edexcel Level 7 BTEC Advanced professional **`Diploma`[EDUCATION_DEGREE]** Management Studies (2010)

**`Post Graduate`[EDUCATION_DEGREE]** Certificate (**`University of Wales`[EDUCATION_INSTITUTE]**) (**`2009`[TENURE]**)

**`Bsc`[EDUCATION_DEGREE]** **`Computers`[EDUCATION_COURSE]** from **`University of Newcastle`[EDUCATION_INSTITUTE]** (**`2006`[TENURE]**)

**`Diploma`[EDUCATION_DEGREE]** in **`Computer Engineering`[EDUCATION_COURSE]**

(**`2003`[TENURE]**)

**`Maharashtra Secondary School`[EDUCATION_INSTITUTE]**, India (**`1997`[TENURE]**)

