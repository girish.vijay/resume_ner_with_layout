Nationality: British

Clearance: SC Cleared

**`Executive Summary`[SUMMARY_HEADING]**

I am a Cisco Certified Network professional with 10+ year experience in thefield

of Telecommunications, IT and Computer Networks of which 4+ years experience

in architectural design, implementation and support of multi-dimensional Network

Projects in public and private sector.

I have a particular Interest in Network and Security Design and Support roles.

I am a highly motivated individual with strong leadership, interpersonal and

negotiation skills who has been responsible for managing, supporting, designing

and implementing cost effective network projects to strict deadlines.

**`Certifications`[OTHERS_HEADING]**

May 2018

Cisco SENSS (Implementing Cisco Edge Security Solutions)

April 2017

PCNSE7 (Palo Alto Certified Network Engineer)

November 2015

Cisco ARCH (Designing Cisco Network Service Architectures)

June 2014

CCIE Routing and Switching (Written)

July 2011

CCIP (Cisco Certified Internetwork Professional)

September 2010

CCNP (Cisco Certified Network Professional)

January 2010

CCNA (Cisco Certified Network Associate)

Technologies

Cisco ASA , Palo Alto and Checkpoint Firewalls.

Cisco 6509 VSS, Cisco ISRs & Cisco Nexus with FEX Technology.

Juniper EX3200 and EX4200

HP5400, HP3800, HP2900 & HP1900 Switches

F5, A10 and Barracuda Load Balancers

DNS, VPNs ,MPLS, SSL & IPSEC VPNs

Cisco Wireless Solution with over 200 APs

Ruckus Wireless Solution with over 100 outdoor APs

Smoothwall Web Filtering Solution

SNMPc Management Platform

Kiwi CatTools, Wireshark, TCPDUMP,Tacacs ,Netphysics, NetFlow, Algosec

etc

**`Work Experience`[WORK_EXPERIENCE_HEADING]**

March **`2016`[TENURE]** Till Date

**`Lead Network Engineer`[EMPLOYER_ROLE]** (Client Specialist)

I have joined **`CenturyLink`[EMPLOYER_COMPANY_NAME]** as a **`Lead Network Engineer`[EMPLOYER_ROLE]** to provide support for

managed hosting customers and to assist and consult for new projects using

**`CenturyLink`[EMPLOYER_COMPANY_NAME]** Platform and Cloud Services.

Responsibilities:

Customer consultation and evaluate growth opportunities for customer

environments.

Creating Design and proof of concept to meet the demands and new

requirements.

Perform incident trend analysis to promote a stable solution within the

environment.

Create and update documentation including network diagrams,

procedures, and policies to ensure consistent support of customer

environments

Review system and network configurations with Service Delivery to ensure

successful implementation of services into production following Lean Best

Practices

Deal Tier 3 and Tier 4 escalations for the issues in the Cloud and Hosting

Environment

Provide guidance to adjacent support tiers to assist in resolution of

complex network, system and security incidents

Clients:

Wallenius Wilhelmsen Logistics

Managed hosting environments in US and EMEA with DR capabilities

Multiple Palo Altofirewall failover pairs managed by centralized Panorama.

Site to site VPN tunnels with external partners.

Remote access vpn using Palo Alto Global Protect

IPS tuning on Palo Alto for all the internet traffic.

Manage other dedicated networking devices F5s, ASA, 3850 & Juniper

stacked switches etc.

Migration over to SD-WAN Cisco Meraki Solution from existing MPLS

Backbone

Lloyds Bank

Provide support for the hosting environment that enables to feed data in to

ProTrade and Arena Market Streams using **`CenturyLink`[EMPLOYER_COMPANY_NAME]** COIN connectivity.

Deployment of Corvil Monitoring taps on various key network points.

Manage dedicated hosted network devices F5 LTM, ASA, Checkpoints,

Juniper Switches and Nexus3k switches.

National Australia Bank

Provide support for the hosting environment that manage clients e-trading

Platforms.

Mange hosting environment with Active and DR sites located in two

Datacentres in different regions.

Manage customer dedicated networking devices that include F5, ASA,

checkpoint juniper and cisco switches.

Optimizing DR solution and making the process automated with minimal

manual intervention.

SkyScanner

Support and Managing colocation and hosting services in US and EMEA using

**`CenturyLink`[EMPLOYER_COMPANY_NAME]** MPLS Backbone and Cloud Infrastructure.

**`Pinacl Solutions`[EMPLOYER_COMPANY_NAME]** (City of York Council)

**`Senior Network Engineer`[EMPLOYER_ROLE]**

Sep **`2011`[TENURE]** Mar **`2016`[TENURE]**.

I am working as a **`Senior Network Engineer`[EMPLOYER_ROLE]** to provide consultancy and support

for network infrastructure. I am responsible for managing, maintaining and

securing City of Yorkfibre network with 8 POP sites that allows them to achieve

high speed connectivity for Corporate, Education and CCTV enterprise network. I

have hands on experience working in Data Centre, Storage, Access, WiFi and

Security Environments. I have also been involved in design and implementation

of network projects using various technologies as a Lead Engineer.

Responsibilities:

Network Monitoring & Management for Education, Corporate and CCTV

Networks.

Technical Lead and point of escalation for all Network and Firewall related

Incidents and Changes.

Ensuring all the systems are patched and backed up on regular intervals

and the syslogs are maintained for all the devices within the network using

various tools

Management of DNS portal by creating and amending A,MX,TXT and

CNAME records.

Design and Implementation of Network and Security solutions and ensure

it meets CoCo and PSN Compliance.

Design and Implementation of WiFi solutions for Libraries, City Wide and

Public Access Sites.

Writing SoW, creating HLD and LLD and ensuring all the documentation is

maintained.

Providing Consultation towards the improvement of the Councils Network

Infrastructure and

upcoming projects.

Projects for Council:

Migration of the Datacentre over to the new Headquarters (WestOffices)

using Cisco 6500 VSS in the core, Nexus 5596 platform for SAN and server farm

and Cisco 2960 stacks for access layer.

Deployment and Configuration of WiFi using Cisco WLC 5508 and 2504

supporting over 300 APs across the council estate.

Migration on Provider Independent Range over two internet links to provide

load balancing and resiliency for internet traffic.

Design and Deployment of the CCTV Network by creating a newfibre ring

around the city using HP3800 switches in the core to migrate UTMC (CCTV, Traffic

Lights and Car Park Counters) legacy circuits over to IP network.

Deployment of Citywide WiFi (Tour de France) using Ruckus Wireless

solution supporting over 100 outdoor APs.

Design of the New PSN (Public Services Network) Network to fulfil PSN

compliance by deploying a new Cisco ASA5545 pair and setting up a new DMZ.

Migration of GCSx Connection to the new Cisco ASA 5545 and setting up the

new policies to enable smooth transition.

Design and Implementation of Disaster Recovery Site to provide resiliency

in the events of outages.

Projects outside Council:

Design and implementation of Remote Access VPN (SSL and WEebVPN

Solution for Benenden Health using Cisco ASA and RSA for dual factor

authentication.

Network and Security Audit for Newcastle under Lyme Council

**`InTechnology`[EMPLOYER_COMPANY_NAME]** UK

**`Network and Voice Engineer`[EMPLOYER_ROLE]**

May **`2008`[TENURE]** August **`2011`[TENURE]**

I was working within the network team at the inTechnology for the Network

Operation Centre where I was responsible for provisioning and incident

management for customers connected to our MPLS network.

Network Support Engineer for Managed Service Provider (ISP) and Unified

Communications (UC).

Responsible for Incidents, problems and Change Management

Managing Voice and Data Services through our MPLS Cloud.

Configuring QoS and implementing Egress and Ingress Policies to pritorise

real time traffic.

Create site to site and remote access VPN Tunnels.

Network support for Public Health Sector customers utilising our N3

connectivity via our MPLS cloud.

Configure Web Filtering on Juniper Firewall by creating custom Profiles and

custom Categories

3rd line support for hosted IP Telephony (Broadsoft Platform).

Configuring Call Centres, Hunt Groups, Auto Attendants and IVR.

Support for Unity Call Centre, Unity Reception and PC Assistant toolbar

Applications.

Support for Unified communications and Integration with Microsoft OCS.

Configuring VoIP handsets (Polycom), IADs and voice routers for OnNet

and OffNet customers.

Management of the core and edge SBCs via the Palladian platform

**`Sitel`[EMPLOYER_COMPANY_NAME]**, Newcastle Upon Tyne, UK

Mar **`2005`[TENURE]** Mar **`2008`[TENURE]**

**`Network Analyst`[EMPLOYER_ROLE]**

I was working as Tier 2 support engineer for BT where we supported BT Managed

LES, Ethernet and ADSL Circuits

2nd Line support for BT Business Customers.

Technical Support for ADSL and VOIP.

Configuration and troubleshooting wide range of Microsoft Applications

including all Email Clients.

Installing, configuring and monitoring the LAN and WAN connections

System/User Administration tasks involving Active Directory and MS

Exchange.

Working through the tickets to manage all the incidents and adhering to

SLA.

Supporting and troubleshooting Cisco routers, Switches and PIX Firewall.

Managing and Troubleshooting VPN Connections.

**`WorldCall Telecom Limited`[EMPLOYER_COMPANY_NAME]**, Karachi, Pakistan

Feb **`2003`[TENURE]** - Oct **`2004`[TENURE]**

**`NOC Transmission Engineer`[EMPLOYER_ROLE]** (24/7)

Alarm Monitoring and Management of Multiservice Access Network (MSAN)

NMS used for maintaining OLT and ONUs: iManager N2000 Fixed Network

Integrated Management System

Monitoring of Local Loop fibre optic ring through OptiX iManager NES

T2000 network monitoring system

Transmission equipment used for MSAN:

OptiX 155/622H (Metro 1000)

STM-1/STM-4 Optical Transmission System

Handling complaints using service centre ticket tracking system and

resolution of

faults without breaching SLA.

**`Education`[EDUCATION_HEADING]**

March **`2006`[TENURE]**

**`MSc`[EDUCATION_DEGREE]** (Master of Science) - **`Data and Telecommunication Systems`[EDUCATION_COURSE]** (1

Year)

**`University of Northumbria at Newcastle Upon Tyne, United Kingdom`[EDUCATION_INSTITUTE]**

February **`2003`[TENURE]**

**`B.E`[EDUCATION_DEGREE]** (Bachelor of Engineering) **`Electrical`[EDUCATION_COURSE]** (4 Years)

**`NED University of Engineering and Technology, Karachi, Pakistan`[EDUCATION_INSTITUTE]**

