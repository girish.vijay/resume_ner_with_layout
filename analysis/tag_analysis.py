import json
import os
import pandas as pd
with open('../data/id_url_map.json', 'r') as f:
    id_url_map = json.load(f)


data_dir = '../data/text_tags'


text_tags = os.listdir(data_dir)
data = {}
for f in text_tags:
    try:
        data[f[:-5]] = json.load(open(data_dir+'/'+ f, "r"))
    except:
        pass

field_seen = []

nowrds = 15
context_tags = {}
for id in data:

    ind  = 0

    while ind < len(data[id]):
        if data[id][ind][1] !='MISC':
            if data[id][ind][1] not in field_seen:
                context_tags[data[id][ind][1]] = {}
                context_tags[data[id][ind][1]]['id'] = []
                context_tags[data[id][ind][1]]['resume_url'] = []
                context_tags[data[id][ind][1]]['context'] = []
                context_tags[data[id][ind][1]]['entity'] = []

                field_seen.append(data[id][ind][1])

            context_tags[data[id][ind][1]]['id'].append(id)
            context_tags[data[id][ind][1]]['resume_url'].append(id_url_map[id])

            cont_left = [t[0] for t in data[id][max(0,ind-5):ind] ]

            txt_left = ''
            if cont_left:
                txt_left = ' '.join(cont_left).split()[-nowrds:]
                txt_left = ' '.join(txt_left)

            txt_right = ''
            cont_right = [t[0] for t in data[id][ind+1:ind +5]]
            if cont_right:
                txt_right = ' '.join(cont_right).split()[:nowrds]
                txt_right = ' '.join(txt_right)
            fin_txt =  txt_left + ' '+data[id][ind][0]+ ' '+ txt_right
            context_tags[data[id][ind][1]]['context'].append(' '.join(fin_txt.split()))
            context_tags[data[id][ind][1]]['entity'].append(data[id][ind][0])


        ind += 1


for tg in context_tags.keys():
    print(tg, len(context_tags[tg]['id']))
    pd.DataFrame(context_tags[tg]).to_csv('tag_context_' + tg +  '.csv')









