import glob
import json
import numpy as np
from collections import Counter
import pandas as pd

tagged_data = '../data/tagged.json'

with open('../data/id_url_map.json', 'r') as f:
    id_url_map = json.load(f)

with open(tagged_data, "r") as f:
    tagged_data = json.load(f)

fils = glob.glob('../data/text_tags/' + "*.json")

dict_miss_extra = dict()
dict_miss_extra['id'] = []
dict_miss_extra['resume_url'] = []

fields = ['name', 'emails_email','phones_phone', 'employer_company_name','employer_role', \
          'education_institute','education_course','education_degree','project_title',\
          'project_company_name']

for i in fields:
    dict_miss_extra[i+'_miss']  = []
    dict_miss_extra[i+'_extra']  = []
    dict_miss_extra[i + '_manual'] = []
    dict_miss_extra[i + '_mapped'] = []

pr = []
indd = 0
for id in tagged_data:
    #print(id,'')
    pth = '../data/text_tags/' + str(id) + ".json"
    if  pth in fils:
        indd+=1
        mapped_data = json.load(open(pth, "r"))

        mapped_keys = [i[1].lower() for i in mapped_data if i[1]!='MISC']
        manual_keys = []
        for tg in tagged_data[id]:
            if tg == 'name':
                manual_keys.append('name')

            elif tg == 'emails':
                manual_keys.extend(['emails_email']*len(tagged_data[id][tg]))

            elif tg == 'phones':
                manual_keys.extend(['phones_phone'] * len(tagged_data[id][tg]))
            elif tg == 'employer':
                for entities in tagged_data[id][tg]:
                    if 'company_name' in entities:
                        manual_keys.append('employer_company_name')
                    if 'role' in  entities:
                        manual_keys.append('employer_role')

            elif tg == 'education':
                for entities in tagged_data[id][tg]:
                    if 'institute' in entities:
                        manual_keys.append('education_institute')
                    if 'course' in  entities:
                        manual_keys.append('education_course')
                    if 'degree' in entities:
                        manual_keys.append('education_degree')

            elif tg == 'project':
                for entities in tagged_data[id][tg]:
                    if 'title' in entities:
                        manual_keys.append('project_title')
                    if 'company' in  entities:
                        manual_keys.append('project_company_name')

        count_manual = Counter(manual_keys)
        count_mapped = Counter(mapped_keys)
        # Difference of list including duplicates
        miss = (count_manual - count_mapped)

        extra = (count_mapped - count_manual)
        dict_miss_extra['id'].append(id)
        dict_miss_extra['resume_url'].append(id_url_map[str(id)])
        for i in fields:

            if i in miss:
                dict_miss_extra[i + '_miss'].append(miss[i])
            else:
                dict_miss_extra[i + '_miss'].append(0)
            if i in extra:
                dict_miss_extra[i + '_extra'].append(extra[i])
            else:
                dict_miss_extra[i + '_extra'].append(0)

            if i in count_manual:
                dict_miss_extra[i + '_manual'].append(count_manual[i])
            else:
                dict_miss_extra[i + '_manual'].append(0)

            if i in count_mapped:
                dict_miss_extra[i + '_mapped'].append(count_mapped[i])
            else:
                dict_miss_extra[i + '_mapped'].append(0)


        pd.DataFrame(dict_miss_extra).to_csv('misses_extra_manual_mapped.csv')
    else:
        print(id, '...')









