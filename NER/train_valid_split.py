import os
import glob
import yaml
import shutil
import numpy as np
import pandas as pd
# set seed for the same train-validation split everytime
np.random.seed(10)


def _train_valid_split(files, valid_ratio=0.2):
    """
    Train and valid split by resume count.
    """
    ixs = np.array(files)
    ixs = np.random.permutation(ixs)
    n_train = int(len(ixs) * (1 - valid_ratio))
    train_ixs, valid_ixs = ixs[:n_train], ixs[n_train:]

    valid_set = sorted([ int(i.split('/') [-1][:-4]) for i in valid_ixs])
    train_set = sorted([int(i.split('/')[-1][:-4]) for i in train_ixs])

    pd.DataFrame(train_set).to_csv('train_set.csv')
    pd.DataFrame(valid_set).to_csv('valid_set.csv')
    # copy files
    for f in train_ixs:
        shutil.copy(f, train_dir + f.split("/")[-1])
    for f in valid_ixs:
        shutil.copy(f, valid_dir + f.split("/")[-1])
    valid_ratio = \
        len(valid_ixs) / (len(train_ixs) + len(valid_ixs))
    print(f"N train resumes: {len(train_ixs)}" +
          f"\nN validation resumes: {len(valid_ixs)}" +
          f"\nvalidation ratio: {round(valid_ratio, 2)}")


if __name__ == "__main__":
    # load config
    with open("../config/conf.yaml", "r") as f:
        conf = yaml.safe_load(f)
    valid_ratio = conf["model"]["validation_ratio"]

    tokenized_dir = "../data/tokenized/"

    train_dir = "../data/train_set/"
    if os.path.exists(train_dir):
        shutil.rmtree(train_dir)
    os.mkdir(train_dir)
    valid_dir = "../data/validation_set/"
    if os.path.exists(valid_dir):
        shutil.rmtree(valid_dir)
    os.mkdir(valid_dir)

    files = glob.glob(tokenized_dir + "*.csv")

    _train_valid_split(files, valid_ratio)
