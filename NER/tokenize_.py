""" Script to tokenize and store all the training data,
to avoid tokenization on the fly. """

import re
import os
import json
from tqdm import tqdm
from transformers import BertTokenizerFast
import pandas as pd

def _tokenize(data, layouts):
    """
    Tokenizes text-tags obtained from parsed and manually tagged
    resume data.
    """


    N = len(data)

    doc_lengths = []
    print("Tokenization ...")
    for i in tqdm(range(N)):
        _id, _data = data[i]
        _idl, _layout = layouts[i]
        _id = _id.split("/")[-1][:-5]
        tokenized_txts, labels, boxes = [], [], []
        for d in _data:
            sent = d[0]
            sent_strt = 1
           # txt = re.sub("\n", " -NL- ", txt)
            for ind, txt in enumerate(sent.split()):
                if ind < len(d[2]):
                    bx = d[2][ind]
                else:
                    try:
                        bx = d[2][-1]
                    except:
                        try:
                            bx = prev_d[-1]
                        except:
                            pass
                prev_d = d[2]
                tokenized_txt = tokenizer.tokenize(txt)
                if tokenized_txt:
                    tokenized_txts += tokenized_txt
                    if d[1] == "MISC":
                        labels += ["MISC"] * len(tokenized_txt)
                        boxes += [bx] * len(tokenized_txt)
                    else:
                        n = len(tokenized_txt)
                        if sent_strt == 1:
                            if n == 1:
                                boxes.append(bx)
                                _label = "B-" + d[1].lower()
                                labels.append(_label)
                            else:
                                boxes += [bx] * n
                                labels.append("B-" + d[1].lower())
                                _label = "I-" + d[1].lower()
                                labels += [_label] * (n - 1)
                        else:
                            _label = "I-" + d[1].lower()
                            labels += [_label] * len(tokenized_txt)
                            boxes += [bx] * len(tokenized_txt)

                    sent_strt = 0
        tokenized_dt = {}
        tokenized_dt['token'] = tokenized_txts
        tokenized_dt['box'] = boxes
        tokenized_dt['label'] = labels

        for i in boxes :
            if i[0]>i[2] or i[1]>i[3]:
                pass
        pd.DataFrame(tokenized_dt).to_csv(tokenized_dir + _id + '.csv')
        '''
        with open(os.path.join(tokenized_dir, _id + ".txt"), "w") as f:
            f.write("\n".join([(t + "\t" + b + "\t" + l)
                    for t, b, l in zip(tokenized_txts, boxes, labels)]))           
        '''

        doc_lengths.append(len(tokenized_txts))


if __name__ == "__main__":
    # load data
    data_dir = "../data/text_tags"
    fixed_data_dir = "../data/text_tags_fixed/"
    tokenized_dir = "../data/tokenized/"

    # load text-tags and override with the manual fixes
    print('\nGenerating file list...')
    text_tags = \
        set(os.listdir(data_dir)).difference(set(os.listdir(fixed_data_dir)))
    text_tags = [os.path.join(data_dir, f) for f in list(text_tags)] + \
        [os.path.join(fixed_data_dir, f) for f in os.listdir(fixed_data_dir)]

    # tokenized data
    print('Loading data...')
    data = []
    layouts = []
    for f in text_tags:
        try:
            f_layout = f.replace('text_tags', 'layout_tags')
            data.append((f, json.load(open(f, "r"))))
            layouts.append((f, json.load(open(f_layout, "r"))))

        except:
            print(f)


    # tokenize
    tokenizer = BertTokenizerFast.from_pretrained('bert-base-cased')
    _tokenize(data, layouts)
