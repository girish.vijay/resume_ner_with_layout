import pandas as pd
import json
import os


tagged_data = '../data/tagged.json'
data_dir = "../data/text_tags"


manual_csv = pd.read_csv('../download_resumes_parse/manual_tagged_analysis.csv')

id_url_map = {i:j  for i, j in zip(manual_csv['id'], manual_csv['resume_url'])}

with open('../data/id_url_map.json', 'w') as f:
    json.dump(id_url_map,f)

with open(tagged_data, "r") as f:
    tagged_data = json.load(f)

text_tags = os.listdir(data_dir)
data = {}
for f in text_tags:
    try:
        data[f[:-5]] = json.load(open(data_dir+'/'+ f, "r"))
    except:
        pass

token_dict = {}
token_dict['id'] = []
token_dict['resume_url'] = []

token_dict['phrase'] = []
token_dict['label'] = []
token_dict['layout'] = []
token_dict['manual_tag'] = []

split_data = int(len(data)/10)

ind_split =1
ind_fil = 1
for _id in data:

    manual_data = tagged_data[_id]
    manual_data_list = []
    for k in manual_data:
        manual_data_list.append(k+'---')
        if type(manual_data[k]) == list:
            for ent_key in manual_data[k]:
                for key in ent_key:
                    if key != 'description':
                        manual_data_list.append(key+'::')
                        manual_data_list.append(ent_key[key])

        elif type(manual_data[k]) == dict:
            for key in manual_data[k]:
                manual_data_list.append(key + ':::')
                try:
                    manual_data_list.append(manual_data[k][key])
                except:
                    pass

        else:
            manual_data_list.append(manual_data[k])





    token_dict['id'].append('')
    token_dict['resume_url'].append('')
    token_dict['phrase'].append('')
    token_dict['label'].append('')
    token_dict['layout'].append('')
    token_dict['manual_tag'].append('')

    ind = 0
    for ent_data in data[_id]:


        token_dict['id'].append(_id)

        if ind == 0:
            token_dict['resume_url'].append(id_url_map[int(_id)])
        else:
            token_dict['resume_url'].append('')

        token_dict['phrase'].append(ent_data[0])
        token_dict['label'].append(ent_data[1])
        token_dict['layout'].append(ent_data[2])

        try:
            token_dict['manual_tag'].append(manual_data_list[ind])
            ind+=1
        except:
            token_dict['manual_tag'].append('')


    while ind < len(manual_data_list):
        token_dict['id'].append(_id)
        token_dict['resume_url'].append('')
        token_dict['phrase'].append('')
        token_dict['label'].append('')
        token_dict['layout'].append('')
        token_dict['manual_tag'].append(manual_data_list[ind])

        ind+=1


    if int(ind_fil) % split_data == 0:
        print(ind_fil, ind_split)
        pd.DataFrame(token_dict).to_csv('../data/training/tagged_manual_' +str(ind_split) + '.csv')
        ind_split += 1
        token_dict = {}
        token_dict['id'] = []
        token_dict['resume_url'] = []
        token_dict['phrase'] = []
        token_dict['label'] = []
        token_dict['layout'] = []
        token_dict['manual_tag'] = []

    ind_fil +=1


print(ind_fil, ind_split)
pd.DataFrame(token_dict).to_csv('../data/training/tagged_manual_' +str(ind_split) + '.csv')





