#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import glob
import pandas as pd
from tqdm import tqdm
from collections import defaultdict

import torch
from torch.utils.data import Dataset
from transformers import DistilBertTokenizerFast


class ResumeDataset(Dataset):
    """
    Resume Dataset(pytorch dataset) for Dataloader.
    """
    def __init__(self, data_dir, seq_len=64, seq_overlap=30):
        self.tokenizer = \
            DistilBertTokenizerFast.from_pretrained("distilbert-base-cased")
        self._dir = data_dir
        self._files = glob.glob(self._dir + "*.csv")
        self.seq_len = seq_len
        self.overlap = seq_overlap
        self._load_data()
        self._sequences = []  # sequences for training
        self.unique_labels = set()
        self._naive_split_with_overlap()
        self.pad_id = self.tokenizer.convert_tokens_to_ids(["PAD"])
        self.label_map = {l: i for i, l in
                          enumerate(sorted(list(self.unique_labels)) + ["PAD"])}

    def _load_data(self):
        self._data = []
        self._file_ids = []
        for i in tqdm(range(len(self._files)), desc="Loading data"):
            _df = pd.read_csv(self._files[i])#, sep="\t", header=None,
                              #quoting=csv.QUOTE_NONE)
            # 1041.pdf, pandas converting "null" string to nan, causing
            # token mapping issues
            _df['token'] = _df['token'].astype(str)
            _df['label'] = _df['label'].astype(str)
            _df = _df[['token', 'box', 'label']]
            self._data.append(_df)
            self._file_ids.append(self._files[i].split('/')[-1][:-4])

    def _naive_split_with_overlap(self):
        """
        Splits each file starting from top using the seq_len, with
        some overlap beteween each consequitive split
        """
        N = len(self._data)
        self._file_to_sequence_map = defaultdict(list)
        seq_no = 0
        for i in tqdm(range(N), desc="Creating sequences"):
            d = self._data[i]
            n = d.shape[0]
            d = d.values
            self.unique_labels.update(set([i[2] for i in d]))
            start_ix = 0
            while True:
                self._file_to_sequence_map[self._file_ids[i]].append(seq_no)
                seq_no += 1
                if start_ix == 0:
                    self._sequences.append(d[start_ix: self.seq_len])
                    if self.seq_len >= n:
                        break
                else:
                    end_ix = start_ix + self.seq_len
                    if end_ix >= n:
                        self._sequences.append(d[n - self.seq_len:])
                        break
                    else:
                        self._sequences.append(d[start_ix: start_ix +
                                                 self.seq_len])
                start_ix += self.seq_len - self.overlap

    def __len__(self):
        return len(self._sequences)

    def __getitem__(self, index):
        tokens = [s[0] for s in self._sequences[index]][:-2]
        boxes = [s[1] for s in self._sequences[index]][:-2]
        labels = [s[2] for s in self._sequences[index]][:-2]
        tokens = ["CLS"] + tokens + ["SEP"]
        labels = ["MISC"] + labels + ["MISC"]
        attn_mask = [1] * len(tokens)
        if len(tokens) < self.seq_len:
            _n = (self.seq_len - len(tokens))
            tokens = tokens + ["PAD"] * _n
            labels = labels + ["PAD"] * _n
            attn_mask += [0] * _n
        token_ids = self.tokenizer.convert_tokens_to_ids(tokens)
        label_ids = [self.label_map[l] for l in labels]

        # check
#         print("**** Manual Check ****")
#         print(f"{"token": <20} | {"tokenId": <10} | {"label": <30} | "
#               f"{"labelId": <10} | {"attnMask": <10}")
#         print("-" * 90)
#         for token, token_id, label, label_id, _attn_mask in \
#             zip(tokens, token_ids, labels, label_ids, attn_mask):
#             print(f"{token: <20} | {token_id: <10} | {label: <30} | "
#                   f"{label_id: <10} | {_attn_mask: <10}")

        if torch.cuda.is_available():
            return torch.Tensor(token_ids).long().cuda(), \
                torch.Tensor(label_ids).long().cuda(), \
                torch.Tensor(attn_mask).long().cuda()
        else:
            return torch.Tensor(token_ids).long(), \
                   torch.Tensor(label_ids).long(), \
                   torch.Tensor(attn_mask).long()
