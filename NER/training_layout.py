#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import yaml
import numpy as np
from tqdm import tqdm
from datetime import datetime
from dataset_layout import ResumeDataset
import torch
from torch.utils.data import DataLoader
from transformers import BertForTokenClassification, AdamW, \
    get_cosine_with_hard_restarts_schedule_with_warmup
from sklearn.metrics import classification_report
from layoutlm import FunsdDataset, LayoutlmConfig, LayoutlmForTokenClassification
from sklearn.metrics import confusion_matrix
import pickle

np.random.seed(11)
model_pth = '../../Layoutlm-OneDrive-2020-06-26'


def train(epoch):
    """
    NER bert model training.
    """
    model.train()
    # Reset the total loss for this epoch.
    total_loss = 0
    # Training loop
    for batch in tqdm(train_dataloader, desc=f"Epoch {epoch + 1} Train"):
        b_input_ids, b_boxes, b_labels, b_input_mask = batch
        # Always clear any previously calculated gradients before
        # performing a backward pass.
        model.zero_grad()
        # forward pass
        outputs = model(b_input_ids, b_boxes, attention_mask=b_input_mask,
                    labels=b_labels)



        # get the loss
        loss = outputs[0]
        # Perform a backward pass to calculate the gradients.
        loss.backward(retain_graph=True)
        # track train loss
        total_loss += loss.item()
        # Clip the norm of the gradient
        # This is to help prevent the "exploding gradients" problem.
        torch.nn.utils.clip_grad_norm_(parameters=model.parameters(),
                                       max_norm=max_grad_norm)
        # update parameters
        optimizer.step()
        # Update the learning rate.
        scheduler.step()

    # Calculate the average loss over the training data.
    avg_train_loss = total_loss / len(train_dataloader)
    # Store the loss value for plotting the learning curve.
    loss_values.append(avg_train_loss)


def validate(epoch):
    """
    NER model validation.
    """
    model.eval()
    # Reset the validation loss for this epoch.
    eval_loss = 0
    predictions, true_labels = [], []
    for batch in tqdm(valid_dataloader, desc=f"Epoch {epoch + 1} Valid"):
        b_input_ids, b_boxes, b_labels, b_input_mask = batch
        # Telling the model not to compute or store gradients,
        # saving memory and speeding up validation
        with torch.no_grad():
            # Forward pass, calculate logit predictions.
            # This will return the logits rather than the loss because
            # we have not provided labels.
            outputs = model(b_input_ids, b_boxes, attention_mask=b_input_mask,
                            labels=b_labels)
        # Move logits and labels to CPU
        logits = outputs[1].detach().cpu().numpy()
        label_ids = b_labels.to("cpu").numpy()

        # Calculate the accuracy for this batch of test sentences.
        eval_loss += outputs[0].item()
        predictions.extend([list(p) for p in np.argmax(logits, axis=2)])
        true_labels.extend(label_ids)

    eval_loss = eval_loss / len(valid_dataloader)
    validation_loss_values.append(eval_loss)

    return true_labels, predictions


if __name__ == "__main__":
    # load config
    with open("../config/conf.yaml", "r") as f:
        conf = yaml.safe_load(f)
    seq_len = conf["model"]["seq_len"]
    seq_overlap = conf["model"]["seq_overlap"]
    valid_ratio = conf["model"]["validation_ratio"]
    batch_size = conf["model"]["batch_size"]

    print("\n**** Data Prep ****")
    # resume dataset(pytorch dataset)
    train_dataset = ResumeDataset("../data/train_set/", seq_len, seq_overlap)
    valid_dataset = ResumeDataset("../data/validation_set/", seq_len, seq_overlap)

    # Dataloader
    train_dataloader = \
        DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    valid_dataloader = \
        DataLoader(valid_dataset, batch_size=batch_size, shuffle=False)


    if  train_dataset.label_map != valid_dataset.label_map:
        train_dataset.unique_labels.update(valid_dataset.unique_labels)
        valid_dataset.unique_labels = train_dataset.unique_labels

        train_dataset.label_map = {l: i for i, l in
                          enumerate(sorted(list(train_dataset.unique_labels)) + ["PAD"])}
        valid_dataset.label_map = train_dataset.label_map

    MODEL_CLASSES = {
        "layoutlm": (LayoutlmConfig, LayoutlmForTokenClassification),
    }

    config_class, model_class = MODEL_CLASSES['layoutlm']
    config = config_class.from_pretrained(model_pth)
    config.num_labels = len(train_dataset.label_map)
    #tokenizer = tokenizer_class.from_pretrained(model_pth)

    model = model_class.from_pretrained(model_pth,
                                        config=config,
                                        )

    # model
    '''

    model = BertForTokenClassification\
        .from_pretrained("bert-base-cased",
                         num_labels=len(train_dataset.label_map),
                         output_attentions=False,
                         output_hidden_states=False)
    '''

    if torch.cuda.is_available():
        model.cuda()  # needs gpu, else skip ethis step and also
        print('....GPU available')
    # make sure dataset in not loading the data on gpu

    # fine-tuning
    param_optimizer = list(model.named_parameters())
    no_decay = ["bias", "gamma", "beta"]
    optimizer_grouped_parameters = \
        [{"params": [p for n, p in param_optimizer
                     if not any(nd in n for nd in no_decay)],
          "weight_decay_rate": 0.01},
         {"params": [p for n, p in param_optimizer
                     if any(nd in n for nd in no_decay)],
          "weight_decay_rate": 0.0}]

    # optimizer
    optimizer = \
        AdamW(optimizer_grouped_parameters, lr=3e-5, eps=1e-8)

    # scheduler
    epochs = conf["model"]["epochs"]
    # total number of training steps is number of batches * number of epochs
    total_steps = len(train_dataloader) * epochs
    # Create the learning rate scheduler.
    scheduler = \
        get_cosine_with_hard_restarts_schedule_with_warmup(
            optimizer,
            num_warmup_steps=0,
            num_training_steps=total_steps,
            num_cycles=5)

    # train the model
    tag_keys = list(train_dataset.label_map.keys())
    max_grad_norm = conf["model"]["max_grad_norm"]

    # n epochs
    print("\n**** Training & Validation ****")
    model_dir = f"../data/models/{datetime.now().strftime('%Y_%m_%d')}"
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)
    loss_values, validation_loss_values = [], []
    validation_data, best_epoch = {}, None
    for i in range(epochs):
        train(i)
        true_labels, predictions = validate(i)
        print(f"Train loss: {loss_values[-1]}" +
              f"\nValid loss: {validation_loss_values[-1]}\n")
        # naive exit
        if i > 0:
            if validation_loss_values[-2] < validation_loss_values[-1]:
                print("\n** Validation loss didn't go down... breaking!!")
                break
        torch.save({
            "epoch": i + 1,
            "model_state_dict": model.state_dict(),
            "train_losses": loss_values,
            "valid_losses": validation_loss_values,
            "tag_keys": tag_keys
            }, f"{model_dir}/{datetime.now().strftime('%Y_%m_%d_h%Hm%M')}_epochs{i + 1}_layout.pt")

        best_epoch = i + 1
        validation_data[best_epoch] = {"true_labels": true_labels,
                                       "predictions": predictions}

    with open("../data/models/tag_keys.json", "w") as f:
        json.dump(tag_keys, f)

    # quantized model
    # model.to("cpu")
    # qmodel = torch.quantization.quantize_dynamic(model, {torch.nn.Linear},
    #                                              dtype=torch.qint8)
    # torch.save({
    #     "epoch": i,
    #     "model_state_dict": qmodel.state_dict(),
    #     "train_losses": loss_values,
    #     "valid_losses": validation_loss_values,
    #     "tag_keys": tag_keys
    #     }, f"data/models/{datetime.now().strftime('%Y_%m_%d_h%Hm%M')}_qint8.pt")

    # classification report
    print('\n**** Classification Report ****')
    pred_tags = [tag_keys[p_i] for p, l in
                 zip(validation_data[best_epoch]["predictions"],
                     validation_data[best_epoch]["true_labels"])
                 for p_i, l_i in zip(p, l) if tag_keys[l_i] != "PAD"]
    true_tags = [tag_keys[l_i] for l in validation_data[best_epoch]["true_labels"]
                 for l_i in l if tag_keys[l_i] != "PAD"]
    print(classification_report(true_tags, pred_tags))
    report = classification_report(true_tags, pred_tags)
    conf_mt = confusion_matrix(true_tags, pred_tags)

    with open('result/confusion_matrix_layout.pickle', 'wb') as f:
        pickle.dump([report, conf_mt] ,f)
