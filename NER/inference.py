import json
import torch
import logging
import numpy as np
from collections import defaultdict
from transformers import BertForTokenClassification, \
    DistilBertTokenizerFast

logger = logging.getLogger("transformers")
logger.setLevel(logging.ERROR)


# TODO: figure out how to use dynamic quantized model _qint8
MODEL = '/home/utsav/Belong/Resume-NER/data/models/2020_06_27/2020_06_27_h12m18_epochs3.pt'


class BertInference():
    def __init__(self, seq_len, overlap):
        self.tokenizer = \
            DistilBertTokenizerFast.from_pretrained("distilbert-base-cased")
        with open("/home/utsav/Belong/Resume-NER/data/models/tag_keys.json", "r") as f:
            self.tag_keys = json.load(f)
        self.model = BertForTokenClassification\
            .from_pretrained("distilbert-base-cased",
                             num_labels=len(self.tag_keys),
                             output_attentions=False,
                             output_hidden_states=False)
        self.seq_len = seq_len
        self.overlap = overlap
        checkpoint = torch.load(MODEL)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.model.eval()

    def _preprocess(self):
        # drop PARSELTONGUE node
        self.txt = ' -NL- '.join(self.parsed_resume_txts)

    def _tokenize(self):
        self.tokenized_txts = self.tokenizer.tokenize(self.txt)

    def _get_sequences(self):
        seq_len = self.seq_len - 2
        overlap = self.overlap

        sequences, overlaps = [], []
        start_ix, n = 0, len(self.tokenized_txts)
        i = 0
        while True:
            if start_ix == 0:
                sequences.append(self.tokenized_txts[start_ix: seq_len])
                if seq_len >= n:
                    break
            else:
                end_ix = start_ix + seq_len
                if end_ix >= n:
                    sequences.append(self.tokenized_txts[n - seq_len:])
                    overlaps.append((i - 1, i,
                                     start_ix + overlap - (n - seq_len)))
                    break
                else:
                    sequences.append(self.tokenized_txts[start_ix:
                                                         start_ix + seq_len])
                    overlaps.append((i - 1, i, overlap))
            start_ix += seq_len - overlap
            i += 1

        self.sequences = sequences
        self.overlaps = overlaps

    def _predict(self):
        sequences = self.sequences
        input_tokens, token_ids, input_masks = [], [], []
        if len(sequences) == 1:
            _input_tokens = ['CLS'] + sequences[0] + ['SEP']
            _input_mask = [1] * len(_input_tokens)
            if len(_input_tokens) < (self.seq_len + 2):
                _n = (self.seq_len + 2 - len(_input_tokens))
                _input_tokens = _input_tokens + ["PAD"] * _n
                _input_mask += [0] * _n
            input_tokens.append(_input_tokens)
            input_masks.append(_input_mask)
        else:
            for s in sequences:
                _input_tokens = ['CLS'] + s + ['SEP']
                _input_mask = [1] * 256
                _token_ids = \
                    self.tokenizer.convert_tokens_to_ids(_input_tokens)
                input_tokens.append(_input_tokens)
                token_ids.append(_token_ids)
                input_masks.append(_input_mask)

        tokens_ids_tensor = torch.Tensor(token_ids).long()
        input_masks_tensor = torch.Tensor(input_masks).long()
        with torch.no_grad():
            output = self.model(tokens_ids_tensor,
                                attention_mask=input_masks_tensor)
        logits = output[0].detach().cpu().numpy()

        logits = logits[:, 1:-1, :]  # remove [CLS] and [SEP]
        non_overlapping_preds = []
        n_preds = len(logits)
        skip_last_mean = False
        for i, p in enumerate(logits):
            if i == 0:
                overlap = self.overlaps[0]
                non_overlapping_preds.append(p[:-1*overlap[2], :])
            elif i == (n_preds - 1):
                overlap = self.overlaps[-1]
                if not skip_last_mean:
                    prev_overlap = self.overlaps[i-1]
                    # mean for the last sequence overlap
                    prev_mean = (logits[i-1][-1*prev_overlap[2]:, :] +
                                p[:prev_overlap[2], :]) / 2
                    non_overlapping_preds.append(prev_mean)
                non_overlapping_preds.append(p[overlap[2]:, :])
            else:
                overlap = self.overlaps[i]
                prev_overlap = self.overlaps[i-1]
                # mean for the last sequence overlap
                prev_mean = (logits[i-1][-1*prev_overlap[2]:, :] +
                             p[:prev_overlap[2], :]) / 2
                non_overlapping_preds.append(prev_mean)
                # current non overlapping sequence
                if (overlap[2] + prev_overlap[2]) > (self.seq_len - 2):
                    non_overlapping_preds.append(p[prev_overlap[2]:, :])
                    skip_last_mean = True
                else:
                    non_overlapping_preds.append(p[prev_overlap[2]:-1*overlap[2], :])

        tokenized_txt_preds = np.concatenate(non_overlapping_preds)
        if len(tokenized_txt_preds) != len(self.tokenized_txts):
            print("**** Tokenization pairing ERROR ****")

        return list(zip(self.tokenized_txts,
                    [self.tag_keys[np.argmax(p)] for p in tokenized_txt_preds]))

    @staticmethod
    def _entities(txt_preds):
        # TODO: check distance bettwen B- and I-
        # TODO: reset txt_so_far if "MISC"
        _txt_preds = [txt_preds[0]]
        txt_preds = txt_preds[1:]
        for i, (txt, pred) in enumerate(txt_preds[:-1]):
            # continuation
            if i > 0 and pred == "MISC" and txt_preds[i-1][1] != "MISC" and \
               (txt_preds[i-1][1][1:] == txt_preds[i+1][1][1:]):
                _txt_preds.append((txt, txt_preds[i+1][1]))
            else:
                _txt_preds.append((txt, pred))
        _txt_preds.append(txt_preds[-1])

        # txt_preds = []
        # n = len(_txt_preds)
        # for i, (txt, pred) in enumerate(_txt_preds):
        #     if i < (n-2) and pred == "MISC" and \
        #         _txt_preds[i+1][1][:2] == "I-" and \
        #        (_txt_preds[i+1][1] == _txt_preds[i+2][1]):
        #         txt_preds.append((txt, "B-" + _txt_preds[i+1][1][2:]))
        #     else:
        #         txt_preds.append((txt, pred))
        txt_preds = _txt_preds

        entities = defaultdict(list)
        in_tag, txt_so_far, lno = None, None, 0
        for i, (txt, pred) in enumerate(txt_preds):
            if txt == "NL" and txt_preds[i-1][0] == "-":
                lno += 1
            if pred != "MISC":
                if txt_so_far:
                    if pred[2:] == in_tag:
                        txt_so_far += " " + txt
                    elif pred[0] == "B":
                        entities[in_tag].append((txt_so_far.replace(" ##", ""), lno))
                        in_tag = pred[2:]
                        txt_so_far = txt
                elif pred[0] == "B":
                    txt_so_far = txt
                    in_tag = pred[2:]
            else:
                if in_tag:
                    entities[in_tag].append((txt_so_far.replace(" ##", ""), lno))
                    in_tag = None
                    txt_so_far = None

        return _txt_preds, entities

    def NER(self, parsed_resume_txts):
        self.parsed_resume_txts = parsed_resume_txts
        self._preprocess()
        self._tokenize()
        self._get_sequences()
        txt_preds = self._predict()
        # entity extraction
        txt_preds, entities = self._entities(txt_preds)
        return txt_preds, entities

    def NERTokens(self, tokenized_txts):
        self.tokenized_txts = tokenized_txts
        self._get_sequences()
        txt_preds = self._predict()
        # entity extraction
        entities = self._entities(txt_preds)
        return txt_preds, entities
