#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Script to parse resumes. """
import sys
sys.path.append('../../parseltongue-ner')

import os
import json
import click
from tqdm import tqdm
from parseltongue.extraction.extract_info import get_text_layout_and_headings

ignore_pdf = ['2901.pdf']

@click.command()
@click.option("--resume-dir", help="input directory for resumes")
@click.option("--parsed-output-dir",
              help="output directory for parsed resumes")
def main(resume_dir: str, parsed_output_dir: str) -> None:
    """
    Parses the downloaded resumes using the Belong parser(Parseltongue NER).



    Arguments:
        resume_dir - input directory for resumes
        parsed_output_dir - output directory for parsed resumes
    """
    if not resume_dir:
        resume_dir = '../data/pdfs'
    if not parsed_output_dir:
        parsed_output_dir = '../data/parsed_pdfs_ner'


    # already parsed
    parsed_pdfs = set([f for f in os.listdir(parsed_output_dir)])
    print(f"\n{len(parsed_pdfs)} pdfs already parsed!")

    # parse and store
    print("Parsing ...")
    pdfs = [f for f in os.listdir(resume_dir) if f.endswith(".pdf")]
    for i in tqdm(range(len(pdfs))):
        if pdfs[i][:-4] + "_ner.json" in parsed_pdfs or  pdfs[i] in ignore_pdf:
            continue
        print(pdfs[i][:-4])
        parsed_info = get_text_layout_and_headings(os.path.join(resume_dir, pdfs[i]))
        if parsed_info:
            with open(os.path.join(parsed_output_dir, pdfs[i][:-4] + "_ner.json"),
                      "w") as f:
                json.dump(parsed_info, f)
    print("Parsed and Written!")


if __name__ == "__main__":
    main()
