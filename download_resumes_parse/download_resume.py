#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Script to download and convert resumes to pdf. """

import os
import json
import click
from tqdm import tqdm
from sys import platform
import pandas as pd

# command to convert resumes to pdf format

if platform == 'darwin':
    CONVERT_TO_PDF = '/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to pdf --outdir {} {}'
else:
    CONVERT_TO_PDF = \
        "/usr/bin/libreoffice --headless --convert-to pdf --outdir {} {} >/dev/null"


def _download_and_convert_resume(url: str, _id: int, out_dir: str,
                                 parsed_pdfs: set) -> bool:
    """
    Downloads resume from the url and converts to pdf if not a pdf already.
    """
    _, resume_name = os.path.split(url)
    resume_ext = resume_name.split(".")[-1]

    is_success = True
    if str(_id) + ".pdf" not in parsed_pdfs:
        out_resume_name = str(_id) + "." + resume_ext
        is_success = \
            os.system(f"wget {url} -O {out_dir}/{out_resume_name}") #+ " >/dev/null 2>&1")

        #is_success = os.system('wget {} -O ../parseltongue-pkg/pdf_manual/temp/{}'.format(url, resume_name))

        if not out_resume_name.endswith(".pdf"):
            is_success = \
                os.system(CONVERT_TO_PDF.format(out_dir, f"{out_dir}/{out_resume_name}"))

    return is_success


@click.command()
@click.option("--manual-data-path", help="path to the manually tagged json")
@click.option("--output-dir", help="path to the output directory for resumes")
def main(manual_data_path: str, output_dir: str) -> None:
    """
    1. Reads the url list from manually tagged json.
    2. Downloads the resume from the url.
    3. Converts to pdf if not a pdf.
    4. For all the resumes which are downloaded and converted successfully,
    the manually tagged data is stored.
    5. The stored manually tagged data is dumped into a json file.

    Arguments:
        manual_data_path - path to the manually tagged json
        output_dir - path to the output directory for resumes
    """
    if not manual_data_path:
        manual_data_path = '../data/manual_data_till_now_.json'
    if not output_dir:
        output_dir = '../data/pdfs'
    # load csv
    print("\nLoading manually tagged json...")
    with open(manual_data_path, "r") as f:
        data = json.load(f)
    print("Loaded!")

    # already downloaded and converted
    parsed_pdfs = set([f for f in os.listdir(output_dir)
                       if f.endswith(".pdf")])
    print(f"\n{len(parsed_pdfs)} pdfs already downloaded and converted!")

    # download resumes and convert to pdf
    print("Downloading resumes and converting to pdf ...")
    n = len(data)
    tagged_data = {}

    dict_resume_details = dict()
    dict_resume_details['resume_url'] = []
    dict_resume_details['id'] = []
    dict_resume_details['source'] = []
    dict_resume_details['manual_fields'] = []
    dict_resume_details['tagged_at'] = []

    for i in tqdm(range(n)):
        url = data[i]["resume_url"]
        manual_data = data[i]["manual_data"]
        id_resume = data[i]['id']
        is_success = \
            _download_and_convert_resume(url, id_resume, output_dir, parsed_pdfs)
        if is_success:
            tagged_data[id_resume] = manual_data
            dict_resume_details['resume_url'].append(url)
            dict_resume_details['id'].append(id_resume)
            dict_resume_details['source'].append(data[i]['source'])
            dict_resume_details['manual_fields'].append(list(manual_data.keys()))
            dict_resume_details['tagged_at'].append(data[i]['tagged_at'])

            # pdfs
    pdfs = [f for f in os.listdir(output_dir) if f.endswith(".pdf")]
    print(f"{len(pdfs)} pdfs found out of {len(data)}")

    pd.DataFrame(dict_resume_details).to_csv('manual_tagged_analysis.csv')
    # tagged data saved in the same directory as the manually tagged json
    with open(os.path.join("../data", "tagged.json"),
              "w") as f:
        json.dump(tagged_data, f)
    print("Output written!!")


if __name__ == "__main__":
    main()
